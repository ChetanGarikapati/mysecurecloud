use securecloud;

insert into users VALUES ('1','akhil');
insert into users VALUES ('2','barath');
insert into users VALUES ('3','chetan');

insert into integrity_levels values (1,'low');
insert into integrity_levels values (2,'medium');
insert into integrity_levels values (3,'high');

INSERT INTO integity_level_relation
(`intergrity_relation_entry_id`,
 `integrity_level`,
 `relation_user_id`,
 `user_id`)
VALUES (2, 1, 3, 1);

INSERT INTO `securecloud`.`integity_level_relation`
(`intergrity_relation_entry_id`,
`integrity_level`,
`relation_user_id`,
`user_id`)
VALUES
(1, 1, 2 , 1);


$(document).ready(function () {

    $("#share-submit").click(function (event) {
        event.preventDefault();

        let url = "/SecureCloudServer_war/securecloud/secure/share/file-share";
        let form = $("#file-share-form")[0];
        let data = new FormData(form);

        $.ajax({
            url: url,
            method: "post",
            enctype: "multipart/form-data",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                alert(data);
            }
        })
    })
})
function processDelete(payload) {
    $.ajax({
        url: "/SecureCloudServer_war/securecloud/secure/file/delete",
        method: "post",
        data: payload,
        success: function (result) {
            console.log(result)
            if (result === "deleted") {
                alert("file deleted");
            } else {
                alert("failed to delete");
            }
        }
    })
}

$("form").on("submit", function (event) {
    event.preventDefault();
    var data = {};
    $(this).serializeArray().map(function (x) {
        data[x.name] = x.value;
    });
    console.log(JSON.parse(JSON.stringify(data)));
    processDelete(data);
});

$(document).ready(function () {
    let params = (new URL(document.location)).searchParams;
    let fileId = params.get("fileId");
    $("#fileId").val(fileId);
})
function createuser(payload) {
    $.ajax({
        url: "/SecureCloudServer_war/securecloud/user/createuser-ui",
        method: "post",
        data: payload,
        success: function (result) {
            console.log(result);
            if (result.userId != -1) {
                alert("user created");
                location.href = "/SecureCloudServer_war/index.html";
            } else {
                alert("creating user failed!");
            }
        }
    })
}

$("form").on("submit", function (event) {
    event.preventDefault();
    var data = {};
    $(this).serializeArray().map(function (x) {
        data[x.name] = x.value;
    });
    console.log(JSON.parse(JSON.stringify(data)));
    createuser(data);
});
function updateFileLabel() {
    $("#file-name-label").html($("#file").val().replace(/^.*[\\\/]/, ''));
}

function enablePasswordField() {
    if ($("#enable-password").is(":checked")) {
        $("#file-password").prop("disabled", false)
    } else {
        $("#file-password").prop("disabled", true)
    }
}

function enablePrivatePasswordField() {
    if ($("#enable-privatekey-password").is(":checked")) {
        $("#private-key-password").prop("disabled", false)
    } else {
        $("#private-key-password").prop("disabled", true)
    }
}

$(document).ready(function () {
    $("#submit-button").click(function (event) {
        event.preventDefault();

        let url = "/SecureCloudServer_war/securecloud/secure/file/upload";
        let form = $("#uploadfile-form")[0];
        let data = new FormData(form);

        $.ajax({
            url: url,
            method: "post",
            enctype: "multipart/form-data",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                alert(data);
            }
        })
    })
})
function toggleFilePassword() {
    if ($("#filePasswordEnabled").is(":checked")) {
        $("#filePassword").prop("disabled", false)
    } else {
        $("#filePassword").prop("disabled", true)
    }
}

$(document).ready(function () {
    let params = (new URL(document.location)).searchParams;
    let fileId = params.get("fileId");
    $("#fileId").val(fileId);
})
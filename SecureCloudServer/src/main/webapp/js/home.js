function getFiles() {

    let downloadUrl = "/SecureCloudServer_war/download.html?fileId=";
    let shareUrl = "/SecureCloudServer_war/sharefile.html?fileId=";
    let deleteUrl = "/SecureCloudServer_war/deletefile.html?fileId=";

    let shareCode = "&nbsp;<span class='badge badge-primary badge-pill'>shared</span>"

    jQuery.ajax({
        url: "/SecureCloudServer_war/securecloud/secure/file/getfiles",
        method: "get",
        success: function (result) {
            console.log(result);
            if (result.length == 0) {
                $("#display-data").append("<h3>No files in drive</h3>");
            }

            for (var i = 0; i < result.length; i++) {
                fileEntry = result[i];
                if (fileEntry.sharedFile == true){
                    $("#display-data").append('<p><a class="btn btn-light btn-outline-dark" href="#" data-toggle="popover" data-placement="top" data-content="<a class=\'btn btn-light\' href=\''+ shareUrl + fileEntry.fileId +'\'>Share</a><a style=\'margin-left:4px\' class=\'btn btn-light\' href=\''+ downloadUrl + fileEntry.fileId +'\'>Download</a><a class=\'btn btn-light\' href=\''+ deleteUrl + fileEntry.fileId +'\'>Delete</a>">' + fileEntry.fileName + shareCode + '</a></p><br>');
                }
                else{
                    $("#display-data").append('<p><a class="btn btn-light btn-outline-dark" href="#" data-toggle="popover" data-placement="top" data-content="<a class=\'btn btn-light\' href=\''+ shareUrl + fileEntry.fileId +'\'>Share</a><a style=\'margin-left:4px\' class=\'btn btn-light\' href=\''+ downloadUrl + fileEntry.fileId +'\'>Download</a><a class=\'btn btn-light\' href=\''+ deleteUrl + fileEntry.fileId +'\'>Delete</a>">' + fileEntry.fileName + '</a></p><br>');
                }
            }

            $('[data-toggle="popover"]').popover({
                html: true,
            });
        }
    });
}

$(document).ready(function () {

    getFiles();

    $('[data-toggle="popover"]').popover({
        html: true,
    });
});
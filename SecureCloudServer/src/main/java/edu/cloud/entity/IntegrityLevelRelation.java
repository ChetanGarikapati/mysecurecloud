package edu.cloud.entity;

import javax.persistence.*;

/**
 * @author Chetan Garikapati
 */
@Entity(name = "integity_level_relation")
@Table(uniqueConstraints = @UniqueConstraint(name = "user_relation", columnNames = {"user_id", "relation_user_id"}))
public class IntegrityLevelRelation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "intergrity_relation_entry_id")
    private int integrityRelationEntryId;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToOne
    @JoinColumn(name = "relation_user_id", nullable = false, referencedColumnName = "user_id")
    private User relationUserId;

    @OneToOne
    @JoinColumn(name = "integrity_level", referencedColumnName = "integrity_level_id", nullable = false)
    private IntegrityLevels integrityLevel;

    public IntegrityLevelRelation() {
    }

    public IntegrityLevelRelation(int integrityRelationEntryId, User user, User relationUserId, int integrityLevel) {
        this.integrityRelationEntryId = integrityRelationEntryId;
        this.user = user;
        this.relationUserId = relationUserId;
        this.integrityLevel = new IntegrityLevels(integrityLevel);
    }

    public IntegrityLevelRelation(int integrityRelationEntryId, User user, User relationUserId, IntegrityLevels integrityLevel) {
        this.integrityRelationEntryId = integrityRelationEntryId;
        this.user = user;
        this.relationUserId = relationUserId;
        this.integrityLevel = integrityLevel;
    }

    public IntegrityLevelRelation(int integrityRelationEntryId) {
        this.integrityRelationEntryId = integrityRelationEntryId;
    }

    public IntegrityLevelRelation(User user, User relationUserId) {
        this.user = user;
        this.relationUserId = relationUserId;
    }

    public IntegrityLevelRelation(int userId, int relationShipUserId) {
        this.user = new User(userId);
        this.relationUserId = new User(relationShipUserId);
    }

    public int getIntegrityRelationEntryId() {
        return integrityRelationEntryId;
    }

    public void setIntegrityRelationEntryId(int integrityRelationEntryId) {
        this.integrityRelationEntryId = integrityRelationEntryId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getRelationUserId() {
        return relationUserId;
    }

    public void setRelationUserId(User relationUserId) {
        this.relationUserId = relationUserId;
    }

    public int getIntegrityLevelValue() {
        return integrityLevel.getIntegrityLevelId();
    }

    public void setIntegrityLevelValue(int integrityLevel) {
        this.integrityLevel = new IntegrityLevels(integrityLevel);
    }

    public IntegrityLevels getIntegrityLevel() {
        return integrityLevel;
    }

    public void setIntegrityLevel(IntegrityLevels integrityLevel) {
        this.integrityLevel = integrityLevel;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IntegrityLevels{");
        sb.append("integrityEntryId=").append(integrityRelationEntryId);
        sb.append(", user=").append(user);
        sb.append(", relationUserId=").append(relationUserId);
        sb.append(", integrityLevel=").append(integrityLevel);
        sb.append('}');
        return sb.toString();
    }
}

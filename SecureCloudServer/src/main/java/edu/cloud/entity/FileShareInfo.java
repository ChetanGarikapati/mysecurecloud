package edu.cloud.entity;

import javax.persistence.*;

@Entity(name = "file_share_info")
@Table(uniqueConstraints = @UniqueConstraint(name = "file_user_relation", columnNames = {"orginal_file_id", "user_id"}))
public class FileShareInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_share_id")
    private int fileShareId;

    @OneToOne
    @JoinColumn(name = "orginal_file_id", nullable = false)
    private FileInfo fileId;

    @OneToOne
    @JoinColumn(name = "shared_file_entry", nullable = false)
    private FileInfo newFileEntry;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User userId;

    @OneToOne
    @JoinColumn(name = "file_owner_user_id", nullable = false)
    private User ownerId;

    public FileShareInfo() {
    }

    public FileShareInfo(int fileShareId) {
        this.fileShareId = fileShareId;
    }

    public FileShareInfo(FileInfo fileId, User userId) {
        this.fileId = fileId;
        this.userId = userId;
    }

    public FileShareInfo(FileInfo fileId, User userId, User ownerId) {
        this.fileId = fileId;
        this.userId = userId;
        this.ownerId = ownerId;
    }

    public FileShareInfo(FileInfo fileId, User userId, User ownerId, FileInfo newFileEntry) {
        this.fileId = fileId;
        this.newFileEntry = newFileEntry;
        this.userId = userId;
        this.ownerId = ownerId;
    }

    public int getFileShareId() {
        return fileShareId;
    }

    public void setFileShareId(int fileShareId) {
        this.fileShareId = fileShareId;
    }

    public FileInfo getFileId() {
        return fileId;
    }

    public void setFileId(FileInfo fileId) {
        this.fileId = fileId;
    }

    public FileInfo getNewFileEntry() {
        return newFileEntry;
    }

    public void setNewFileEntry(FileInfo newFileEntry) {
        this.newFileEntry = newFileEntry;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }
}

package edu.cloud.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * @author Chetan Garikapati
 */
@Entity(name = "files")
public class FileInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private int fileId;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private User user;

    @Column(name = "file_type", nullable = false)
    private String fileType;

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Column(name = "file_location", nullable = false)
    @JsonIgnore
    private String fileLocation;

    @Column(name = "file_size", nullable = false)
    private long fileSize;

    @Column(name = "file_password")
    @JsonIgnore
    private String filePassword;

    @Column(name = "password_protection_enabled")
    @JsonIgnore
    private boolean passwordProtectionEnabled;

    @Lob
    @Column(name = "secret_key")
    @JsonIgnore
    private String secretKey;

    @Lob
    @Column(name = "initialization_vector")
    @JsonIgnore
    private String IV;

    @Lob
    @Column(name = "salt")
    @JsonIgnore
    private String salt;

    @Column(name = "secret_key_algo", nullable = false)
    @JsonIgnore
    private String secretKeyAlgorithm;

    @Column(name = "is_shared")
    private boolean sharedFile;

    public FileInfo() {
    }

    public FileInfo(int fileId, User user) {
        this.fileId = fileId;
        this.user = user;
    }

    public FileInfo(int fileId) {
        this.fileId = fileId;
    }

    public FileInfo(int fileId, int userId) {
        this.fileId = fileId;
        this.user = new User(userId);
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePassword() {
        return filePassword;
    }

    public void setFilePassword(String filePassword) {
        this.filePassword = filePassword;
    }

    public boolean isPasswordProtectionEnabled() {
        return passwordProtectionEnabled;
    }

    public void setPasswordProtectionEnabled(boolean passwordProtectionEnabled) {
        this.passwordProtectionEnabled = passwordProtectionEnabled;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @JsonIgnore
    public String getIV() {
        return IV;
    }

    public void setIV(String IV) {
        this.IV = IV;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getSecretKeyAlgorithm() {
        return secretKeyAlgorithm;
    }

    public void setSecretKeyAlgorithm(String secretKeyAlgorithm) {
        this.secretKeyAlgorithm = secretKeyAlgorithm;
    }

    public boolean isSharedFile() {
        return sharedFile;
    }

    public void setSharedFile(boolean sharedFile) {
        this.sharedFile = sharedFile;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FileInfo{");
        sb.append("fileId=").append(fileId);
        sb.append(", user=").append(user.getUserId());
        sb.append(", fileType='").append(fileType).append('\'');
        sb.append(", fileName='").append(fileName).append('\'');
        sb.append(", fileLocation='").append(fileLocation).append('\'');
        sb.append(", fileSize=").append(fileSize);
        sb.append('}');
        return sb.toString();
    }
}

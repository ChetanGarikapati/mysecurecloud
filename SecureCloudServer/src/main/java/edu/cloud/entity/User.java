package edu.cloud.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Chetan Garikapati
 */
@Entity(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;

    @Column(name = "user_name", nullable = false, unique = true)
    private String userName;

    @Column(name = "nick_name", nullable = false)
    private String nickName;

    @JsonIgnore
    @Column(name = "user_directory", nullable = false)
    private UUID userDirectory;

    public User() {
    }

    public User(int userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public User(int userId, String userName, String nickName) {
        this.userId = userId;
        this.userName = userName;
        this.nickName = nickName;
    }

    public User(String userName, String nickName) {
        this.userName = userName;
        this.nickName = nickName;
    }

    public User(int userId) {
        this.userId = userId;
    }

    public User(String userName) {
        this.userName = userName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public UUID getUserDirectory() {
        return userDirectory;
    }

    public void setUserDirectory(UUID userDirectory) {
        this.userDirectory = userDirectory;
    }

    @PrePersist
    public void createUserDirectoryName() {
        if (userDirectory == null) {
            userDirectory = UUID.randomUUID();
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("userId=").append(userId);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", nickName='").append(nickName).append('\'');
        sb.append(", userDirectory=").append(userDirectory);
        sb.append('}');
        return sb.toString();
    }
}

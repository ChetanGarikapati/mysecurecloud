package edu.cloud.entity;

import javax.persistence.*;

@Entity(name = "user_keys")
public class UserKeys {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_keys_id")
    private int userKeysId;

    @Lob
    @Column(name = "public_key", nullable = false)
    private String publicKey;

    @Lob
    @Column(name = "private_key", nullable = false)
    private String privateKey;

    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id", nullable = false)
    private User userId;

    @Lob
    @Column(name = "privatekey_salt",nullable = false)
    private String privateKeySalt;

    @Column(name = "privatekey_password", nullable = false)
    private String privateKeyPassword;

    @Lob
    @Column(name = "iv",nullable = false)
    private String IV;

    public UserKeys() {
    }

    public UserKeys(int userKeysId, String publicKey, String privateKey, User userId) {
        this.userKeysId = userKeysId;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.userId = userId;
    }

    public UserKeys(User userId) {
        this.userId = userId;
    }

    public UserKeys(String publicKey, String privateKey, User userId) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.userId = userId;
    }

    public int getUserKeysId() {
        return userKeysId;
    }

    public void setUserKeysId(int userKeysId) {
        this.userKeysId = userKeysId;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public void setPrivateKeyPassword(String privateKeyPassword) {
        this.privateKeyPassword = privateKeyPassword;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getPrivateKeySalt() {
        return privateKeySalt;
    }

    public void setPrivateKeySalt(String privateKeySalt) {
        this.privateKeySalt = privateKeySalt;
    }

    public String getIV() {
        return IV;
    }

    public void setIV(String IV) {
        this.IV = IV;
    }
}

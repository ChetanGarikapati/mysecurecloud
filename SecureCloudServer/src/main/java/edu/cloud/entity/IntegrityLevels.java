package edu.cloud.entity;

import javax.persistence.*;

/**
 * @author Chetan Garikapati
 */
@Entity(name = "integrity_levels")
public class IntegrityLevels {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "integrity_level_id")
    private int integrityLevelId;

    @Column(name = "integrity_value_description", nullable = false)
    private String integrityValueDescription;

    public IntegrityLevels() {
    }

    public IntegrityLevels(int integrityLevelId, String integrityValueDescription) {
        this.integrityLevelId = integrityLevelId;
        this.integrityValueDescription = integrityValueDescription;
    }

    public IntegrityLevels(int integrityLevelId) {
        this.integrityLevelId = integrityLevelId;
    }

    public int getIntegrityLevelId() {
        return integrityLevelId;
    }

    public void setIntegrityLevelId(int integrityLevelId) {
        this.integrityLevelId = integrityLevelId;
    }

    public String getIntegrityValueDescription() {
        return integrityValueDescription;
    }

    public void setIntegrityValueDescription(String integrityValueDescription) {
        this.integrityValueDescription = integrityValueDescription;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IntegrityLevels{");
        sb.append("integrityLevelId=").append(integrityLevelId);
        sb.append(", integrityValueDescription='").append(integrityValueDescription).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

package edu.cloud.entity;

import javax.persistence.*;

/**
 * @author Chetan Garikapati
 */
@Entity(name = "passwords")
public class Passwords {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "password_id")
    private int passwordId;

    @Column(name = "password", nullable = false)
    private String hashedPassword;

    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id", nullable = false, unique = true)
    private User userId;

    public Passwords() {
    }

    public Passwords(int passwordId) {
        this.passwordId = passwordId;
    }

    public Passwords(int passwordId, String hashedPassword, int userId) {
        this.passwordId = passwordId;
        this.hashedPassword = hashedPassword;
        this.userId = new User(userId);
    }

    public Passwords(int passwordId, String hashedPassword, User userId) {
        this.passwordId = passwordId;
        this.hashedPassword = hashedPassword;
        this.userId = userId;
    }

    public int getPasswordId() {
        return passwordId;
    }

    public void setPasswordId(int passwordId) {
        this.passwordId = passwordId;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Passwords{");
        sb.append("passwordId=").append(passwordId);
        sb.append(", hashedPassword='").append(hashedPassword).append('\'');
        sb.append(", userId=").append(userId);
        sb.append('}');
        return sb.toString();
    }
}

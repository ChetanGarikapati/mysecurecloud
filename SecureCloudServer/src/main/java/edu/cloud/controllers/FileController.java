package edu.cloud.controllers;

import edu.cloud.entity.FileInfo;
import edu.cloud.security.components.SecureCloudUserDetails;
import edu.cloud.services.FileDeleteService;
import edu.cloud.services.FileDisplayService;
import edu.cloud.services.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static edu.cloud.services.FileStorageService.DEFAULT_OUTPUT_DIRECTORY_NAME;

/**
 * Rest controller with endpoints to support file upload,download and delete operations
 *
 * @author Chetan Garikapati
 */
@RestController
@RequestMapping("/secure/file")
public class FileController {

    private FileStorageService fileStorageService;
    private FileDisplayService fileDisplayService;
    private FileDeleteService fileDeleteService;
    private File saveFileDirectory;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class);

    @Autowired
    public FileController(FileStorageService fileStorageService, File saveFileDirectory,
                          FileDeleteService fileDeleteService, FileDisplayService fileDisplayService) {
        this.fileStorageService = fileStorageService;
        this.saveFileDirectory = saveFileDirectory;
        this.fileDisplayService = fileDisplayService;
        this.fileDeleteService = fileDeleteService;
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file, @RequestParam Optional<String> filePassword,
                             @RequestParam Optional<String> enablePassword, @RequestParam Optional<String> privateKeyPassword) {
        if (!file.isEmpty() && privateKeyPassword.isEmpty()) {
            int saveFileStatus = fileStorageService.saveFile(file, filePassword, enablePassword, getCurrentUserInfo());
            if (saveFileStatus != -1 && saveFileStatus != -2) {
                return "File saved";
            } else if (saveFileStatus == -2) {
                return "File Already exists provide private key password to overwrite";
            }
        } else if (privateKeyPassword.isPresent()) {
            boolean handleExistingFileSave = fileStorageService.handleExistingFileSave(file, getCurrentUserInfo(), privateKeyPassword.get());
            if (handleExistingFileSave) return "File saved/Overwritten";
            return "Could not store the file";
        }
        return "Saving File failed";
    }

    @PostMapping("/downloadfile")
    public ResponseEntity<Resource> downloadFile(@RequestParam String fileId, @RequestParam String privateKeyPassword, @RequestParam Optional<String> filePassword) {
        try {
            if (!fileId.isEmpty()) {
                Optional<FileInfo> files = fileStorageService.downloadFile(fileId, getCurrentUserInfo(), privateKeyPassword, filePassword);
                if (files.isPresent()) {
                    Path outputFileLocation = Path.of(saveFileDirectory.getAbsolutePath(), getCurrentUserInfo().getStorageDirectory(), DEFAULT_OUTPUT_DIRECTORY_NAME, files.get().getFileName());
                    System.out.println(outputFileLocation);
                    return ResponseEntity.ok()
                            .contentType(MediaType.parseMediaType(files.get().getFileType()))
                            .contentLength(files.get().getFileSize())
                            .cacheControl(CacheControl.noCache())
                            .header(HttpHeaders.CONTENT_DISPOSITION, new StringBuilder().append("attachement; ")
                                    .append("filename=").append(files.get().getFileName()).toString())
                            .body(new FileSystemResource(outputFileLocation));
                }
            }
        } catch (Exception e) {
            LOGGER.error("Downloading file failed : {}", e);
            return ResponseEntity.ok().body(new ByteArrayResource("failed to download".getBytes()));
        }
        return null;
    }

    @GetMapping("/getfiles")
    public List<FileInfo> getUserFiles() {
        Optional<List<FileInfo>> allFilesOfUser = fileDisplayService.getAllFilesOfUser(getCurrentUserInfo());
        if (allFilesOfUser.isPresent()) return allFilesOfUser.get();
        return new ArrayList<>();
    }

    @PostMapping("/delete")
    public String deleteFile(@RequestParam String fileId, @RequestParam String privateKeyPassword) {
        if (!fileId.isEmpty() && !privateKeyPassword.isEmpty()) {
            boolean deleteStatus = fileDeleteService.processFileDeletion(fileId, privateKeyPassword, getCurrentUserInfo());
            if (deleteStatus) return "deleted";
        }
        return "failed delete";
    }

    private SecureCloudUserDetails getCurrentUserInfo() {
        return (SecureCloudUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}

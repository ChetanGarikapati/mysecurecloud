package edu.cloud.controllers;

import edu.cloud.components.UserLogin;
import edu.cloud.entity.User;
import edu.cloud.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Rest controller with end points for user operations.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private UserServices userServices;

    @Autowired
    public UserController(UserServices userServices) {
        this.userServices = userServices;
    }

    @PostMapping(value = "/createuser")
    public User createUser(@RequestBody UserLogin userLogin) {
        Optional<User> checkValidUser = userServices.createUser(userLogin);
        if (checkValidUser.isPresent()) return checkValidUser.get();
        return new User(-1, "Creating User Failed");
    }

    @PostMapping("/createuser-ui")
    public User createUserUI(@RequestParam String userName, @RequestParam Optional<String> nickName,
                             @RequestParam String password, @RequestParam String privateKeyPassword) {
        UserLogin userLogin = new UserLogin();
        nickName.ifPresent(nickNameValid -> userLogin.setNickName(nickNameValid));
        userLogin.setPassword(password);
        userLogin.setPrivateKeyPassword(privateKeyPassword);
        userLogin.setUserName(userName);
        Optional<User> checkValidUser = userServices.createUser(userLogin);
        if (checkValidUser.isPresent()) return checkValidUser.get();
        return new User(-1, "Creating User Failed");
    }


}

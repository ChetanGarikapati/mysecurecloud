package edu.cloud.controllers;

import edu.cloud.security.components.SecureCloudUserDetails;
import edu.cloud.services.FileShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Rest controller with endpoints to provide file sharing.
 *
 * @author Chetan Garikapati
 */
@RestController
@RequestMapping("/secure/share")
public class FileShareController {

    private FileShareService fileShareService;

    @Autowired
    public FileShareController(FileShareService fileShareService) {
        this.fileShareService = fileShareService;
    }

    @PostMapping("/file-share")
    public String shareFileWithUser(@RequestParam String fileId, @RequestParam String recipientEmail, @RequestParam String privateKeyPassword,
                                    @RequestParam Optional<String> filePassword) {

        boolean shareSuccessful = fileShareService.processFileShare(fileId, recipientEmail, privateKeyPassword, filePassword, getCurrentUserInfo());
        if (shareSuccessful)
            return "File Shared with : " + recipientEmail;
        return "Failed sharing";
    }

    private SecureCloudUserDetails getCurrentUserInfo() {
        return (SecureCloudUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}

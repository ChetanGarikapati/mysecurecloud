package edu.cloud.services;

import edu.cloud.dao.FileDAO;
import edu.cloud.entity.FileInfo;
import edu.cloud.entity.User;
import edu.cloud.security.components.SecureCloudUserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Helper service to load all user files on home page.
 *
 * @author Chetan Garikapati
 */
@Service
public class FileDisplayService {

    private FileDAO fileDAO;

    public FileDisplayService(FileDAO fileDAO) {
        this.fileDAO = fileDAO;
    }

    public Optional<List<FileInfo>> getAllFilesOfUser(SecureCloudUserDetails secureCloudUserDetails) {
        return fileDAO.findAllByUser(new User(secureCloudUserDetails.getUserId()));
    }
}

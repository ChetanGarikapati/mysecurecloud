package edu.cloud.services;

import edu.cloud.dao.FileDAO;
import edu.cloud.dao.FileShareInfoDAO;
import edu.cloud.dao.UserDAO;
import edu.cloud.entity.FileInfo;
import edu.cloud.entity.FileShareInfo;
import edu.cloud.entity.User;
import edu.cloud.entity.UserKeys;
import edu.cloud.security.components.SecureCloudUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

import static edu.cloud.services.EncryptionAndDecryptionService.KEYS_ENCRYPTION_ALGORITM;

/**
 * This service class provide file sharing capabilities.
 *
 * @author Chetan Garikapati
 */
@Service
public class FileShareService {

    private FileDAO fileDAO;
    private FileShareInfoDAO fileShareInfoDAO;
    private UserDAO userDAO;
    private SCryptPasswordEncoder sCryptPasswordEncoder;
    private UserServices userServices;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileShareService.class);

    @Autowired
    public FileShareService(FileDAO fileDAO, FileShareInfoDAO fileShareInfoDAO, UserDAO userDAO, SCryptPasswordEncoder sCryptPasswordEncoder, UserServices userServices) {
        this.fileDAO = fileDAO;
        this.fileShareInfoDAO = fileShareInfoDAO;
        this.userDAO = userDAO;
        this.sCryptPasswordEncoder = sCryptPasswordEncoder;
        this.userServices = userServices;
    }

    public boolean processFileShare(String fileId, String recipientEmail, String privateKeyPassword, Optional<String> filePassword, SecureCloudUserDetails currentUserInfo) {

        try {
            Optional<FileInfo> fileInfo = fileDAO.findById(Integer.valueOf(fileId));
            Optional<User> recipientUser = userDAO.findByUserName(recipientEmail);
            Optional<FileShareInfo> fileShareInfoOptional = fileShareInfoDAO.findByFileIdAndUserId(new FileInfo(Integer.valueOf(fileId)), new User(currentUserInfo.getUserId()));

            if (fileInfo.isPresent() && !privateKeyPassword.isEmpty() && recipientUser.isPresent()) {
                if (fileInfo.get().isPasswordProtectionEnabled() && filePassword.isPresent()
                        && sCryptPasswordEncoder.matches(filePassword.get(), fileInfo.get().getFilePassword())) {
                    return shareFile(fileInfo, recipientUser, fileShareInfoOptional, privateKeyPassword, currentUserInfo);
                } else if (!fileInfo.get().isPasswordProtectionEnabled()) {
                    return shareFile(fileInfo, recipientUser, fileShareInfoOptional, privateKeyPassword, currentUserInfo);
                }
            }
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Failed Sharing file, NoSuchAlgorithmException", e);
            return false;
        } catch (InvalidKeyException e) {
            LOGGER.error("Failed Sharing file, InvalidKeyException", e);
            return false;
        } catch (NoSuchPaddingException e) {
            LOGGER.error("Failed Sharing file, NoSuchPaddingException", e);
            return false;
        } catch (BadPaddingException e) {
            LOGGER.error("Failed Sharing file, BadPaddingException", e);
            return false;
        } catch (InvalidKeySpecException e) {
            LOGGER.error("Failed Sharing file, InvalidKeySpecException", e);
            return false;
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("Failed Sharing file, IllegalBlockSizeException", e);
            return false;
        } catch (Exception e) {
            LOGGER.error("Failed Sharing file, Unkown Error occured", e);
            return false;
        }

        return false;
    }

    private boolean shareFile(Optional<FileInfo> fileInfo, Optional<User> recipientUser, Optional<FileShareInfo> fileShareInfoOptional, String privateKeyPassword, SecureCloudUserDetails currentUserInfo) throws Exception {
        Optional<PrivateKey> usersDecryptedPrivateKey = userServices.getUsersDecryptedPrivateKey(new User(currentUserInfo.getUserId()), privateKeyPassword);
        if (usersDecryptedPrivateKey.isPresent()) {
            Base64.Decoder decoder = Base64.getDecoder();
            Cipher keyEncryptionCipher = Cipher.getInstance(KEYS_ENCRYPTION_ALGORITM);
            keyEncryptionCipher.init(Cipher.DECRYPT_MODE, usersDecryptedPrivateKey.get());

            byte[] secretKeyBytes = keyEncryptionCipher.doFinal(decoder.decode(fileInfo.get().getSecretKey()));
            byte[] IVBytes = keyEncryptionCipher.doFinal(decoder.decode(fileInfo.get().getIV()));
            byte[] saltBytes = null;
            if (fileInfo.get().isPasswordProtectionEnabled()) {
                saltBytes = keyEncryptionCipher.doFinal(decoder.decode(fileInfo.get().getSalt()));
            }

            Optional<UserKeys> userPublicKey = userServices.getUserPublicKey(recipientUser.get());
            if (userPublicKey.isPresent()) {
                byte[] decodedPublicKey = Base64.getDecoder().decode(userPublicKey.get().getPublicKey());
                PublicKey publicKey = KeyFactory.getInstance(KEYS_ENCRYPTION_ALGORITM).generatePublic(new X509EncodedKeySpec(decodedPublicKey));
                keyEncryptionCipher.init(Cipher.ENCRYPT_MODE, publicKey);
                Base64.Encoder encoder = Base64.getEncoder();

                FileInfo sharedFileInfo = new FileInfo();
                sharedFileInfo.setPasswordProtectionEnabled(fileInfo.get().isPasswordProtectionEnabled());
                sharedFileInfo.setFileSize(fileInfo.get().getFileSize());
                sharedFileInfo.setSecretKeyAlgorithm(fileInfo.get().getSecretKeyAlgorithm());
                sharedFileInfo.setFileLocation(fileInfo.get().getFileLocation());
                sharedFileInfo.setFileType(fileInfo.get().getFileType());
                sharedFileInfo.setFileName(fileInfo.get().getFileName());
                sharedFileInfo.setFilePassword(fileInfo.get().getFilePassword());
                sharedFileInfo.setUser(recipientUser.get());
                sharedFileInfo.setSharedFile(true);

                sharedFileInfo.setSecretKey(encoder.encodeToString(keyEncryptionCipher.doFinal(secretKeyBytes)));
                sharedFileInfo.setIV(encoder.encodeToString(keyEncryptionCipher.doFinal(IVBytes)));
                if (saltBytes != null)
                    sharedFileInfo.setSalt(encoder.encodeToString(keyEncryptionCipher.doFinal(saltBytes)));

                FileInfo persistedSharedFile = fileDAO.save(sharedFileInfo);
                if (fileShareInfoOptional.isEmpty()) {
                    FileShareInfo fileShareInfo = fileShareInfoDAO.save(new FileShareInfo(fileInfo.get(), recipientUser.get(), new User(currentUserInfo.getUserId()), persistedSharedFile));
                }
                return true;
            }
        }

        return false;
    }
}



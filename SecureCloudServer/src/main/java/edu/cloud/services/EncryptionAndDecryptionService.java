package edu.cloud.services;

import edu.cloud.components.EncryptionContainer;
import edu.cloud.entity.FileInfo;
import edu.cloud.entity.User;
import edu.cloud.entity.UserKeys;
import edu.cloud.security.components.SecureCloudUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Path;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

import static edu.cloud.services.FileStorageService.DEFAULT_OUTPUT_DIRECTORY_NAME;

/**
 * This service class provides encryption and decryption service of files.
 *
 * @author Chetan Garikapati
 */
@Service
public class EncryptionAndDecryptionService {

    private SecureRandom secureRandom;
    private UserServices userServices;
    private File saveFileDirectory;

    private SecretKeyFactory secretKeyFactory;
    private Cipher cipher;
    private Cipher keyEncryptionCipher;
    private KeyGenerator keyGenerator;
    private SecretKey secretKey;

    private static final String ENCRYPTION_ALGORITHM = "AES";
    static final String KEYS_ENCRYPTION_ALGORITM = "RSA";
    private static final String CIPHER = "AES/GCM/NoPadding";
    private static final String KEY_LENGTH_DERIVATION_ALOGORITHM = "PBKDF2WithHmacSHA256";

    private static final int ITERATION_COUNT = 1000;
    private static final int KEYSIZE = 256;
    private static final int SALT_LENGTH = 16;
    private static final int GCM_IV_LENGTH = 12;
    private static final int GCM_TAG_LENGTH = 128;
    private static final int BUFFER_SIZE_LIMIT = 1024;

    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionAndDecryptionService.class);

    @Autowired
    public EncryptionAndDecryptionService(SecureRandom secureRandom, File saveFileDirectory, UserServices userServices) {
        this.secureRandom = secureRandom;
        this.saveFileDirectory = saveFileDirectory;
        this.userServices = userServices;

        try {
            keyGenerator = KeyGenerator.getInstance(ENCRYPTION_ALGORITHM);
            keyGenerator.init(KEYSIZE);
            secretKeyFactory = SecretKeyFactory.getInstance(KEY_LENGTH_DERIVATION_ALOGORITHM);
            cipher = Cipher.getInstance(CIPHER);
            keyEncryptionCipher = Cipher.getInstance(KEYS_ENCRYPTION_ALGORITM);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.error("Creating Encryption Service Failed, Saving files will fail : {}", e);
        }
    }

    public boolean encryptAndSaveFile(String outputFileLocation, Optional<String> filePassword, Optional<String> enablePassword, MultipartFile file, SecureCloudUserDetails secureCloudUserDetails, FileInfo fileToSave) {
        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(outputFileLocation));
             BufferedInputStream bufferedInputStream = new BufferedInputStream(file.getInputStream())
        ) {
            EncryptionContainer encryptionContainer = new EncryptionContainer();
            byte[] IV = secureRandom.generateSeed(GCM_IV_LENGTH);

            if (enablePassword.isPresent() && filePassword.isPresent()) {
                encryptionContainer.setSalt(secureRandom.generateSeed(SALT_LENGTH));
                PBEKeySpec pbeKeySpec = new PBEKeySpec(filePassword.get().toCharArray(), encryptionContainer.getSalt(), ITERATION_COUNT, KEYSIZE);
                secretKey = secretKeyFactory.generateSecret(pbeKeySpec);
            } else {
                secretKey = keyGenerator.generateKey();
            }

            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), ENCRYPTION_ALGORITHM);
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, IV);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, gcmParameterSpec);

            int available;
            byte[] input;

            while (bufferedInputStream.available() > 0) {
                available = bufferedInputStream.available();
                input = bufferedInputStream.readNBytes(BUFFER_SIZE_LIMIT);

                if (available > BUFFER_SIZE_LIMIT) {
                    byte[] encryptedPartial = cipher.update(input);
                    bufferedOutputStream.write(encryptedPartial);
                } else {
                    byte[] encryptedFinal = cipher.doFinal(input);
                    bufferedOutputStream.write(encryptedFinal);
                }
            }

            bufferedOutputStream.flush();
            fileToSave.setSecretKeyAlgorithm(secretKey.getAlgorithm());
            encryptionContainer.setIV(IV);
            encryptionContainer.setSecretKey(secretKey);
            boolean encryptSecretKeys = encryptSecretKeys(encryptionContainer, secureCloudUserDetails.getUserId(), fileToSave);
            return encryptSecretKeys;

        } catch (FileNotFoundException e) {
            LOGGER.error("File not found, ecryption failed : {}", e);
            return false;
        } catch (IOException e) {
            LOGGER.error("Filed storing file, ecryption failed : {}", e);
            return false;
        } catch (InvalidAlgorithmParameterException e) {
            LOGGER.error("Invalid Algorithm Parameter, ecryption failed : {}", e);
            return false;
        } catch (InvalidKeyException e) {
            LOGGER.error("Invalid Algorithm Parameter, ecryption failed : {}", e);
            return false;
        } catch (BadPaddingException e) {
            LOGGER.error("Padding length for algorithm is invalid, ecryption failed : {}", e);
            return false;
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("Invalid block size, ecryption failed : {}", e);
            return false;
        } catch (InvalidKeySpecException e) {
            LOGGER.error("Invalid keyspec passed,encryption failed : {}", e);
            return false;
        }
    }

    private boolean encryptSecretKeys(EncryptionContainer encryptionContainer, int userId, FileInfo fileToSave) {
        Optional<UserKeys> userPublicKey = userServices.getUserPublicKey(new User(userId));
        if (userPublicKey.isEmpty()) return false;

        try {
            byte[] decodedPublicKey = Base64.getDecoder().decode(userPublicKey.get().getPublicKey());
            PublicKey publicKey = KeyFactory.getInstance(KEYS_ENCRYPTION_ALGORITM).generatePublic(new X509EncodedKeySpec(decodedPublicKey));
            keyEncryptionCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            Base64.Encoder encoder = Base64.getEncoder();

            fileToSave.setSecretKey(encoder.encodeToString(keyEncryptionCipher.doFinal(encryptionContainer.getSecretKey().getEncoded())));
            fileToSave.setIV(encoder.encodeToString(keyEncryptionCipher.doFinal(encryptionContainer.getIV())));
            if (encryptionContainer.getSalt() != null)
                fileToSave.setSalt(encoder.encodeToString(keyEncryptionCipher.doFinal(encryptionContainer.getSalt())));
            return true;
        } catch (Exception e) {
            LOGGER.error("Failed encrypting secret keys", e);
            return false;
        }
    }

    public boolean encryptAndSaveExistingFile(FileInfo fileInfo, MultipartFile file, SecureCloudUserDetails userDetails, String privateKeyPassword) {

        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(fileInfo.getFileLocation()));
             BufferedInputStream bufferedInputStream = new BufferedInputStream(file.getInputStream())
        ) {
            Base64.Decoder decoder = Base64.getDecoder();
            Optional<PrivateKey> usersDecryptedPrivateKey = userServices.getUsersDecryptedPrivateKey(new User(userDetails.getUserId()), privateKeyPassword);
            if (usersDecryptedPrivateKey.isPresent()) {
                keyEncryptionCipher.init(Cipher.DECRYPT_MODE, usersDecryptedPrivateKey.get());
                byte[] secretKeyBytes = keyEncryptionCipher.doFinal(decoder.decode(fileInfo.getSecretKey()));
                byte[] IVBytes = keyEncryptionCipher.doFinal(decoder.decode(fileInfo.getIV()));

                SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, ENCRYPTION_ALGORITHM);
                GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, IVBytes);
                Cipher cipher = Cipher.getInstance(CIPHER);
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, gcmParameterSpec);

                int available;
                byte[] input;

                while (bufferedInputStream.available() > 0) {
                    available = bufferedInputStream.available();
                    input = bufferedInputStream.readNBytes(BUFFER_SIZE_LIMIT);

                    if (available > BUFFER_SIZE_LIMIT) {
                        byte[] encryptedPartial = cipher.update(input);
                        bufferedOutputStream.write(encryptedPartial);
                    } else {
                        byte[] encryptedFinal = cipher.doFinal(input);
                        bufferedOutputStream.write(encryptedFinal);
                    }
                }

                bufferedOutputStream.flush();
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("Failed encrypting and saving existing file", e);
            return false;
        }
        return false;
    }

    public boolean decryptAndDownloadFile(FileInfo fileInfo, SecureCloudUserDetails userDetails, String privateKeyPassword, Optional<UserKeys> userKeys) {
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(fileInfo.getFileLocation()));
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(saveFileDirectory + File.separator + userDetails.getStorageDirectory() + File.separator + DEFAULT_OUTPUT_DIRECTORY_NAME + File.separator + fileInfo.getFileName()))
        ) {
            File decryptedFile = new File(String.valueOf(Path.of(saveFileDirectory + File.separator + userDetails.getStorageDirectory() + File.separator + DEFAULT_OUTPUT_DIRECTORY_NAME + File.separator + fileInfo.getFileName())));
            if (decryptedFile.exists()) decryptedFile.delete();

            Base64.Decoder decoder = Base64.getDecoder();
            Optional<PrivateKey> usersDecryptedPrivateKey = userServices.getUsersDecryptedPrivateKey(new User(userDetails.getUserId()), privateKeyPassword);

            if (usersDecryptedPrivateKey.isPresent()) {
                keyEncryptionCipher.init(Cipher.DECRYPT_MODE, usersDecryptedPrivateKey.get());

                byte[] secretKeyBytes = keyEncryptionCipher.doFinal(decoder.decode(fileInfo.getSecretKey()));
                byte[] IVBytes = keyEncryptionCipher.doFinal(decoder.decode(fileInfo.getIV()));

                SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, ENCRYPTION_ALGORITHM);
                GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, IVBytes);
                cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, gcmParameterSpec);

                int available;
                while (bufferedInputStream.available() > 0) {
                    available = bufferedInputStream.available();
                    byte[] input = bufferedInputStream.readNBytes(BUFFER_SIZE_LIMIT);

                    if (available > BUFFER_SIZE_LIMIT) {
                        byte[] decryptedPartial = cipher.update(input);
                        bufferedOutputStream.write(decryptedPartial);
                    } else {
                        byte[] decryptedFinal = cipher.doFinal(input);
                        bufferedOutputStream.write(decryptedFinal);
                    }
                }

                bufferedOutputStream.flush();
                return true;
            }
        } catch (Exception e) {
            LOGGER.error("Failed decrypting and downloading of file", e);
            return false;
        }

        return false;
    }
}

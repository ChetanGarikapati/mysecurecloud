package edu.cloud.services;

import edu.cloud.components.UserLogin;
import edu.cloud.dao.AccountInfoDAO;
import edu.cloud.dao.PasswordDAO;
import edu.cloud.dao.UserDAO;
import edu.cloud.dao.UserKeysDAO;
import edu.cloud.entity.AccountInfo;
import edu.cloud.entity.Passwords;
import edu.cloud.entity.User;
import edu.cloud.entity.UserKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

/**
 * This class provides various user operations
 *
 * @author Chetan Garikapati
 */
@Service
public class UserServices {

    private UserDAO userDAO;
    private PasswordDAO passwordDAO;
    private UserKeysDAO userKeysDAO;
    private AccountInfoDAO accountInfoDAO;

    private KeyPairGenerator KEY_PAIR_GENERATOR;
    private SCryptPasswordEncoder sCryptPasswordEncoder;
    private SecureRandom secureRandom;

    public static final int SALT_LENGTH = 16;
    public static final int KEYSIZE = 256;
    public static final int RSA_KEY_LENGTH = 2048;
    public static final String KEY_PAIR_ALGORITHM = "RSA";
    public static final String KEY_PAIR_ENCRYPTION_ALGORITHM = "AES";
    public static final String KEY_PAIR_ENCRYPTION_CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    public static final String KEY_LENGTH_DERIVATION_ALOGORITHM = "PBKDF2WithHmacSHA256";

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServices.class);

    @Autowired
    public UserServices(UserDAO userDAO, PasswordDAO passwordDAO, AccountInfoDAO accountInfoDAO,
                        UserKeysDAO userKeysDAO, SCryptPasswordEncoder sCryptPasswordEncoder, SecureRandom secureRandom) {

        this.userDAO = userDAO;
        this.passwordDAO = passwordDAO;
        this.accountInfoDAO = accountInfoDAO;
        this.userKeysDAO = userKeysDAO;
        this.sCryptPasswordEncoder = sCryptPasswordEncoder;
        this.secureRandom = secureRandom;

        try {
            KEY_PAIR_GENERATOR = KeyPairGenerator.getInstance(KEY_PAIR_ALGORITHM);
            KEY_PAIR_GENERATOR.initialize(RSA_KEY_LENGTH);
        } catch (Exception e) {
            LOGGER.error("Failed creating keypair generator, new users cannot be created : {}", e);
        }
    }

    @Transactional
    public Optional<User> createUser(UserLogin userLogin) {
        if ((userLogin != null && userDAO.existsByUserName(userLogin.getUserName())) || userLogin.getPassword().isEmpty()) {
            User newUser = new User(-1, "User with this email already exists / password is empty");
            return Optional.ofNullable(newUser);
        } else {
            User newUser = userDAO.save(new User(userLogin.getUserName(), userLogin.getNickName()));
            if (newUser != null) {
                Passwords newUserPasswords = new Passwords(0, sCryptPasswordEncoder.encode(userLogin.getPassword()), newUser);
                AccountInfo accountInfo = new AccountInfo(newUser);
                Optional<UserKeys> userKeys = generateKeyPairForUser(newUser, userLogin.getPrivateKeyPassword());

                if (userKeys.isPresent()) {
                    passwordDAO.save(newUserPasswords);
                    accountInfoDAO.save(accountInfo);
                    userKeysDAO.save(userKeys.get());
                    return Optional.of(newUser);
                }
                userDAO.delete(newUser);
                Optional.empty();
            }
        }
        return Optional.empty();
    }

    private Optional<UserKeys> generateKeyPairForUser(User user, String privateKeyPassword) {
        try {
            Base64.Encoder encoder = Base64.getEncoder();
            UserKeys userKeys = new UserKeys();
            userKeys.setUserId(user);

            KeyPair keyPair = KEY_PAIR_GENERATOR.generateKeyPair();
            PrivateKey privateKey = keyPair.getPrivate();
            PublicKey publicKey = keyPair.getPublic();

            byte[] salt = secureRandom.generateSeed(SALT_LENGTH);
            PBEKeySpec pbeKeySpec = new PBEKeySpec(privateKeyPassword.toCharArray(), salt, 5000, KEYSIZE);
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(KEY_LENGTH_DERIVATION_ALOGORITHM);
            SecretKey secretKey = secretKeyFactory.generateSecret(pbeKeySpec);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), KEY_PAIR_ENCRYPTION_ALGORITHM);
            Cipher cipher = Cipher.getInstance(KEY_PAIR_ENCRYPTION_CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

            String encryptedPrivateKey = encoder.encodeToString(cipher.doFinal(privateKey.getEncoded()));
            String encodedPublicKey = encoder.encodeToString(publicKey.getEncoded());

            userKeys.setPublicKey(encodedPublicKey);
            userKeys.setPrivateKey(encryptedPrivateKey);
            userKeys.setPrivateKeyPassword(sCryptPasswordEncoder.encode(privateKeyPassword));
            userKeys.setPrivateKeySalt(encoder.encodeToString(salt));
            userKeys.setIV(encoder.encodeToString(cipher.getIV()));

            return Optional.ofNullable(userKeys);

        } catch (BadPaddingException e) {
            LOGGER.error("Failed generating key pair for user, BadPaddingException", e);
            return Optional.empty();
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("Failed generating key pair for user, IllegalBlockSizeException", e);
            return Optional.empty();
        } catch (InvalidKeyException e) {
            LOGGER.error("Failed generating key pair for user, InvalidKeyException", e);
            return Optional.empty();
        } catch (InvalidKeySpecException e) {
            LOGGER.error("Failed generating key pair for user, InvalidKeySpecException", e);
            return Optional.empty();
        } catch (NoSuchPaddingException e) {
            LOGGER.error("Failed generating key pair for user, NoSuchPaddingException", e);
            return Optional.empty();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Failed generating key pair for user, NoSuchAlgorithmException", e);
            return Optional.empty();
        }

    }

    public Optional<UserKeys> getUserPublicKey(User user) {
        return userKeysDAO.findByUserId(user);
    }

    public Optional<PrivateKey> getUsersDecryptedPrivateKey(User user, String privateKeyPassword) {
        try {
            Optional<UserKeys> userKeys = userKeysDAO.findByUserId(user);
            if (userKeys.isPresent() && sCryptPasswordEncoder.matches(privateKeyPassword, userKeys.get().getPrivateKeyPassword())) {
                Base64.Decoder decoder = Base64.getDecoder();
                byte[] salt = decoder.decode(userKeys.get().getPrivateKeySalt());

                PBEKeySpec pbeKeySpec = new PBEKeySpec(privateKeyPassword.toCharArray(), salt, 5000, KEYSIZE);
                SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(KEY_LENGTH_DERIVATION_ALOGORITHM);
                SecretKey secretKey = secretKeyFactory.generateSecret(pbeKeySpec);
                SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), KEY_PAIR_ENCRYPTION_ALGORITHM);
                Cipher cipher = Cipher.getInstance(KEY_PAIR_ENCRYPTION_CIPHER_ALGORITHM);
                cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(decoder.decode(userKeys.get().getIV())));

                byte[] decryptedPrivateKeyBytes = cipher.doFinal(decoder.decode(userKeys.get().getPrivateKey()));
                PrivateKey privateKey = KeyFactory.getInstance(KEY_PAIR_ALGORITHM).generatePrivate(new PKCS8EncodedKeySpec(decryptedPrivateKeyBytes));

                return Optional.of(privateKey);
            }
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return Optional.empty();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            return Optional.empty();
        } catch (BadPaddingException e) {
            e.printStackTrace();
            return Optional.empty();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return Optional.empty();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            return Optional.empty();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}

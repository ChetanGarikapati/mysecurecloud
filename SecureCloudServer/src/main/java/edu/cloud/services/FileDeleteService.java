package edu.cloud.services;

import edu.cloud.dao.FileDAO;
import edu.cloud.dao.FileShareInfoDAO;
import edu.cloud.entity.FileInfo;
import edu.cloud.entity.FileShareInfo;
import edu.cloud.entity.User;
import edu.cloud.security.components.SecureCloudUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.PrivateKey;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * This service class handles file deletion of normal and shared files.
 *
 * @author Chetan Garikapati
 */
@Service
public class FileDeleteService {

    private FileDAO fileDAO;
    private FileShareInfoDAO fileShareInfoDAO;
    private UserServices userServices;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileDeleteService.class);

    @Autowired
    public FileDeleteService(FileDAO fileDAO, FileShareInfoDAO fileShareInfoDAO, UserServices userServices) {
        this.fileDAO = fileDAO;
        this.fileShareInfoDAO = fileShareInfoDAO;
        this.userServices = userServices;
    }

    public boolean processFileDeletion(String fileId, String privateKeyPassword, SecureCloudUserDetails secureCloudUserDetails) {
        try {
            Optional<PrivateKey> usersDecryptedPrivateKey = userServices.getUsersDecryptedPrivateKey(new User(secureCloudUserDetails.getUserId()), privateKeyPassword);
            Optional<FileInfo> existingFile = fileDAO.findById(Integer.valueOf(fileId));
            if (usersDecryptedPrivateKey.isPresent() && existingFile.isPresent()) {
                Set<FileShareInfo> sharedFilesList = fileShareInfoDAO.findByFileId(existingFile.get());
                Set<FileInfo> filesToDelete = new HashSet<>();
                if (!sharedFilesList.isEmpty()) {
                    sharedFilesList.forEach(fileShareInfo -> {
                        filesToDelete.add(new FileInfo(fileShareInfo.getNewFileEntry().getFileId()));
                    });

                    fileShareInfoDAO.deleteAll(sharedFilesList);
                    fileDAO.deleteAll(filesToDelete);
                    fileDAO.delete(existingFile.get());
                } else {
                    fileDAO.delete(existingFile.get());
                }
                return true;
            }

        } catch (Exception e) {
            LOGGER.error("Failed deleting the files", e);
            return false;
        }
        return false;
    }
}

package edu.cloud.services;

import edu.cloud.dao.FileDAO;
import edu.cloud.dao.FileShareInfoDAO;
import edu.cloud.dao.UserKeysDAO;
import edu.cloud.entity.FileInfo;
import edu.cloud.entity.FileShareInfo;
import edu.cloud.entity.User;
import edu.cloud.entity.UserKeys;
import edu.cloud.security.components.SecureCloudUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

/**
 * This service class provide upload file functionality.
 *
 * @author Chetan Garikapati
 */
@Service
public class FileStorageService {

    private FileDAO fileDAO;
    private FileShareInfoDAO fileShareInfoDAO;
    private UserKeysDAO userKeysDAO;
    private File saveFileDirectory;
    private EncryptionAndDecryptionService encryptionAndDecryptionService;

    private SCryptPasswordEncoder sCryptPasswordEncoder;

    public static final String DEFAULT_OUTPUT_DIRECTORY_NAME = "decrypted";
    private static final Logger LOGGER = LoggerFactory.getLogger(FileStorageService.class);

    @Autowired
    public FileStorageService(FileDAO fileDAO, UserKeysDAO userKeysDAO, FileShareInfoDAO fileShareInfoDAO,
                              File saveFileDirectory, EncryptionAndDecryptionService encryptionAndDecryptionService, SCryptPasswordEncoder sCryptPasswordEncoder) {

        this.fileDAO = fileDAO;
        this.userKeysDAO = userKeysDAO;
        this.fileShareInfoDAO = fileShareInfoDAO;
        this.saveFileDirectory = saveFileDirectory;
        this.encryptionAndDecryptionService = encryptionAndDecryptionService;
        this.sCryptPasswordEncoder = sCryptPasswordEncoder;
    }

    public int saveFile(MultipartFile file, Optional<String> filePassword, Optional<String> enablePassword, SecureCloudUserDetails secureCloudUserDetails) {
        String sanitizedFilePath = StringUtils.cleanPath(file.getOriginalFilename());

        Optional<FileInfo> existingFile = checkExistingFile(file.getOriginalFilename(), secureCloudUserDetails.getUserId());
        if (existingFile.isPresent()) return -2;

        FileInfo fileToSave = new FileInfo();
        fileToSave.setFileType(file.getContentType());
        fileToSave.setUser(new User(secureCloudUserDetails.getUserId()));
        fileToSave.setFileName(sanitizedFilePath);
        fileToSave.setFileSize(file.getSize());

        File saveFileLocation = new File(saveFileDirectory, secureCloudUserDetails.getStorageDirectory() + File.separator + sanitizedFilePath);
        System.out.println("Saved file to :  " + saveFileLocation);

        try {
            Files.createDirectories(Path.of(saveFileDirectory.getAbsolutePath(), secureCloudUserDetails.getStorageDirectory()));
            Files.createDirectories(Path.of(saveFileDirectory.getAbsolutePath(), secureCloudUserDetails.getStorageDirectory(), DEFAULT_OUTPUT_DIRECTORY_NAME));
            //Files.createFile(Path.of(saveFileLocation.getAbsolutePath()));
            fileToSave.setFileLocation(saveFileLocation.getAbsolutePath());

            if (enablePassword.isPresent() && filePassword.isPresent()) {
                fileToSave.setFilePassword(sCryptPasswordEncoder.encode(filePassword.get()));
                fileToSave.setPasswordProtectionEnabled(true);
            } else {
                fileToSave.setPasswordProtectionEnabled(false);
            }

            boolean encryptAndSaveFile = encryptionAndDecryptionService
                    .encryptAndSaveFile(saveFileLocation.getAbsolutePath(), filePassword, enablePassword, file, secureCloudUserDetails, fileToSave);

            if (encryptAndSaveFile) {
                return fileDAO.save(fileToSave).getFileId();
            }
            return -1;
        } catch (FileNotFoundException e) {
            LOGGER.error("Saving file failed,File not found : {}", e);
            return -1;
        } catch (IOException e) {
            LOGGER.error("Error saving file : {}", e);
            return -1;
        }

    }

    public Optional<FileInfo> checkExistingFile(String fileName, int userId) {
        Optional<FileInfo> existingFile = fileDAO.findByFileNameAndUser(fileName, new User(userId));
        return existingFile;
    }

    public boolean handleExistingFileSave(MultipartFile file, SecureCloudUserDetails secureCloudUserDetails, String privateKeyPassword) {
        try {
            Optional<FileInfo> existingFile = checkExistingFile(file.getOriginalFilename(), secureCloudUserDetails.getUserId());
            if (existingFile.isPresent()) {
                Files.delete(Path.of(existingFile.get().getFileLocation()));
                existingFile.get().setFileSize(file.getSize());
                encryptionAndDecryptionService.encryptAndSaveExistingFile(existingFile.get(), file, secureCloudUserDetails, privateKeyPassword);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            LOGGER.error("Failed saving existing file", e);
            return false;
        }
    }

    public Optional<FileInfo> downloadFile(String fileId, SecureCloudUserDetails currentUserInfo, String privateKeyPassword, Optional<String> filePassword) {

        Optional<FileInfo> existingFile = fileDAO.findById(Integer.valueOf(fileId));
        Optional<FileShareInfo> fileShareInfo = fileShareInfoDAO.findByFileIdAndUserId(new FileInfo(Integer.valueOf(fileId)), new User(currentUserInfo.getUserId()));

        File userDownloadLocation = new File(String.valueOf(Path.of(saveFileDirectory.getAbsolutePath(), currentUserInfo.getStorageDirectory(), DEFAULT_OUTPUT_DIRECTORY_NAME)));
        if (!userDownloadLocation.exists()){
            userDownloadLocation.mkdirs();
        }

        if (existingFile.isPresent() && (existingFile.get().getUser().getUserId() == currentUserInfo.getUserId() ||
                (fileShareInfo.isPresent() && fileShareInfo.get().getUserId().getUserId() == currentUserInfo.getUserId()))
                && !privateKeyPassword.isEmpty()) {
            Optional<UserKeys> userKeys = userKeysDAO.findByUserId(new User(currentUserInfo.getUserId()));

            if (userKeys.isPresent() && sCryptPasswordEncoder.matches(privateKeyPassword, userKeys.get().getPrivateKeyPassword())) {
                boolean decryptionStatus = false;
                if (existingFile.get().isPasswordProtectionEnabled() && filePassword.isPresent() && sCryptPasswordEncoder.matches(filePassword.get(), existingFile.get().getFilePassword())) {
                    decryptionStatus = encryptionAndDecryptionService.decryptAndDownloadFile(existingFile.get(), currentUserInfo, privateKeyPassword, userKeys);
                } else if (!existingFile.get().isPasswordProtectionEnabled()) {
                    decryptionStatus = encryptionAndDecryptionService.decryptAndDownloadFile(existingFile.get(), currentUserInfo, privateKeyPassword, userKeys);
                }
                if (decryptionStatus) return existingFile;
            }
        }

        return Optional.empty();
    }
}

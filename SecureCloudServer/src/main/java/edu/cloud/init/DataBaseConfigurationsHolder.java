package edu.cloud.init;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * This is placeholder configuration class where DB properties are loaded from config files
 * and stored at runtime.
 *
 * @author Chetan Garikapati
 */
@Configuration
@PropertySource(value = "classpath:db.properties")
public class DataBaseConfigurationsHolder {

    private String username;
    private String password;
    private String driverClass;
    private String url;

    public String getUsername() {
        return username;
    }

    @Value("${database.username}")
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    @Value("${database.password}")
    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClass() {
        return driverClass;
    }

    @Value("${database.driverclass}")
    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getUrl() {
        return url;
    }

    @Value("${database.url}")
    public void setUrl(String url) {
        this.url = url;
    }
}

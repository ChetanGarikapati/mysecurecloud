package edu.cloud.init;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.io.File;

/**
 * Bootstrap class that creates spring container on server startup
 * Use caution when changing values here.
 *
 * @author Chetan Garikapati
 */
public class ServerSpringContainerInitialization implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext secureCloudApplicationContext = new AnnotationConfigWebApplicationContext();
        secureCloudApplicationContext.register(InitializationConfigurations.class);

        servletContext.addListener(new ContextLoaderListener(secureCloudApplicationContext));
        ServletRegistration.Dynamic cloudDispatcherServlet = servletContext.addServlet("Cloud_Dispatcher_Servlet",
                new DispatcherServlet(secureCloudApplicationContext));
        cloudDispatcherServlet.addMapping("/securecloud/*");
        cloudDispatcherServlet.setLoadOnStartup(1);

        //Adding support for uploading file
        File uploadDirectory = new File(System.getProperty("java.io.tmpdir"));
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(uploadDirectory.getAbsolutePath());

        cloudDispatcherServlet.setMultipartConfig(multipartConfigElement);
    }
}

package edu.cloud.init;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.File;
import java.util.Properties;

/**
 * Base configuration class for spring container and secure cloud application <br>
 * Add customizations in this class if required
 *
 * @author Chetan Garikapati
 */
@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan("edu.cloud")
@EnableJpaRepositories(
        entityManagerFactoryRef = "EntityManagerFactoryBean",
        basePackages = "edu.cloud",
        transactionManagerRef = "JpaTransactionManager")
public class InitializationConfigurations {

    private static final String BASE_PACKAGES = "edu.cloud";

    @Bean
    public HibernateJpaVendorAdapter setupJpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public DriverManagerDataSource setupDataSource(DataBaseConfigurationsHolder dataBaseConfigurationsHolder) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataBaseConfigurationsHolder.getDriverClass());
        dataSource.setUrl(dataBaseConfigurationsHolder.getUrl());
        dataSource.setUsername(dataBaseConfigurationsHolder.getUsername());
        dataSource.setPassword(dataBaseConfigurationsHolder.getPassword());
        return dataSource;
    }

    @Bean(name = "EntityManagerFactoryBean")
    public LocalContainerEntityManagerFactoryBean setupEntityManagerFactory(JpaVendorAdapter jpaVendorAdapter, DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setPackagesToScan(BASE_PACKAGES);
        entityManagerFactoryBean.setPersistenceUnitName("SecureCloud-Persistence-Unit");
        entityManagerFactoryBean.setJpaProperties(additionalJPAProperties());
        return entityManagerFactoryBean;
    }

    @Bean(name = "JpaTransactionManager")
    public JpaTransactionManager setupTransactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    public SCryptPasswordEncoder getScrpytHashEncoder() {
        return new SCryptPasswordEncoder();
    }

    @Bean(name = "fileSaveDirectory")
    public File getDirectoryToSaveFiles() {
        return new File(System.getProperty("java.io.tmpdir"));
    }

    private Properties additionalJPAProperties() {
        Properties additionalJpaProperties = new Properties();
        additionalJpaProperties.put("javax.persistence.schema-generation.database.action", "drop-and-create");
        return additionalJpaProperties;
    }

}

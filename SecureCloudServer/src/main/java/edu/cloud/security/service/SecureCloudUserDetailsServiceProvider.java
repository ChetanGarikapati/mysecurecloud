package edu.cloud.security.service;

import edu.cloud.dao.AccountInfoDAO;
import edu.cloud.dao.PasswordDAO;
import edu.cloud.dao.UserDAO;
import edu.cloud.entity.AccountInfo;
import edu.cloud.entity.Passwords;
import edu.cloud.entity.User;
import edu.cloud.security.components.SecureCloudUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

/**
 * Service to get user details from JPA and provides to spring security
 *
 * @author Chetan Garikapati
 */
@Service
public class SecureCloudUserDetailsServiceProvider implements UserDetailsService {

    private UserDAO userDAO;
    private PasswordDAO passwordDAO;
    private AccountInfoDAO accountInfoDAO;

    @Autowired
    public SecureCloudUserDetailsServiceProvider(UserDAO userDAO, PasswordDAO passwordDAO, AccountInfoDAO accountInfoDAO) {
        this.userDAO = userDAO;
        this.passwordDAO = passwordDAO;
        this.accountInfoDAO = accountInfoDAO;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return getSecureCloudUserDetail(s);
    }

    private UserDetails getSecureCloudUserDetail(String username) {
        Optional<User> validUser = userDAO.findByUserName(username);
        if (validUser.isPresent()) {
            Optional<Passwords> validPassword = passwordDAO.findByUserId(validUser.get());
            Optional<AccountInfo> validAccountInfo = accountInfoDAO.findByUserId(validUser.get());

            SecureCloudUserDetails secureCloudUserDetails = new SecureCloudUserDetails();
            secureCloudUserDetails.setUserId(validUser.get().getUserId());
            secureCloudUserDetails.setUsername(validUser.get().getUserName());
            secureCloudUserDetails.setPassword(validPassword.get().getHashedPassword());
            secureCloudUserDetails.setStorageDirectory(validUser.get().getUserDirectory().toString());
            secureCloudUserDetails.setAccountNonExpired(validAccountInfo.get().isAccountNonExpired());
            secureCloudUserDetails.setEnabled(validAccountInfo.get().isEnabled());
            secureCloudUserDetails.setAccountNonLocked(validAccountInfo.get().isAccountNonLocked());
            secureCloudUserDetails.setCredentialsNonExpired(validAccountInfo.get().isCredentialsNonExpired());
            secureCloudUserDetails.setGrantedAuthorities(Arrays.asList(new SimpleGrantedAuthority("USER")));

            return secureCloudUserDetails;
        }
        return null;
    }
}

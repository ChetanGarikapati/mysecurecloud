package edu.cloud.security.service;

import edu.cloud.security.components.SecureCloudUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

import static edu.cloud.services.FileStorageService.DEFAULT_OUTPUT_DIRECTORY_NAME;

/**
 * Service to delete decrypted files after user logout.
 *
 * @author Chetan Garikapati
 */
@Service
public class DeleteDecryptedFolderService {

    private File saveFileDirectory;
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteDecryptedFolderService.class);

    @Autowired
    public DeleteDecryptedFolderService(File saveFileDirectory) {
        this.saveFileDirectory = saveFileDirectory;
    }

    public void deleteDecryptedFiles(SecureCloudUserDetails secureCloudUserDetails) {
        try {
            File decryptedFolder = new File(saveFileDirectory.getAbsolutePath() + secureCloudUserDetails.getStorageDirectory() + DEFAULT_OUTPUT_DIRECTORY_NAME);
            if (decryptedFolder.exists() && decryptedFolder.isDirectory()) {
                decryptedFolder.delete();
            }
            LOGGER.info("Decryption Folder of user deleted");
        } catch (Exception e) {
            LOGGER.error("Failed deleting decrypted files ", e);
        }
    }
}

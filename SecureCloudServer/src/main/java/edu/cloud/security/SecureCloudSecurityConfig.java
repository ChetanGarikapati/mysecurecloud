package edu.cloud.security;

import edu.cloud.security.components.SecureCloudUserDetails;
import edu.cloud.security.service.DeleteDecryptedFolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

import java.security.SecureRandom;

/**
 * Base configuration class of spring security <br>
 * Add customizations in this class if required.
 *
 *
 * @author Chetan Garikapati
 */
@Configuration
@EnableWebSecurity
public class SecureCloudSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final SCryptPasswordEncoder sCryptPasswordEncoder;
    private final DeleteDecryptedFolderService deleteDecryptedFolderService;

    @Autowired
    public SecureCloudSecurityConfig(UserDetailsService userDetailsService, SCryptPasswordEncoder sCryptPasswordEncoder,
                                     DeleteDecryptedFolderService deleteDecryptedFolderService) {
        this.userDetailsService = userDetailsService;
        this.sCryptPasswordEncoder = sCryptPasswordEncoder;
        this.deleteDecryptedFolderService = deleteDecryptedFolderService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .usernameParameter("userName")
                .passwordParameter("password")
                .loginPage("/index.html")
                .loginProcessingUrl("/securecloud/user/login")
                .permitAll()
                .successHandler((httpServletRequest, httpServletResponse, authentication) -> {
                    System.out.println("Authorities: " + authentication.getAuthorities());
                    System.out.println(httpServletRequest.getContextPath());
                    httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/homepage.html");
                })
                .and()
                .logout()
                .logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> {
                    SecureCloudUserDetails userDetails = (SecureCloudUserDetails) authentication.getPrincipal();
                    deleteDecryptedFolderService.deleteDecryptedFiles(userDetails);
                    httpServletResponse.sendRedirect("/SecureCloudServer_war/index.html");
                });

        http.httpBasic();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/securecloud/user/createuser").and().ignoring()
                .antMatchers("/createaccount.html")
                .antMatchers("/js/createuser.js")
                .antMatchers("/securecloud/user/createuser-ui");
    }

    @Bean
    public SecureRandom prepareSecureRandomInstance() {
        return new SecureRandom();
    }

}

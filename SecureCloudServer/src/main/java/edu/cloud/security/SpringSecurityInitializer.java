package edu.cloud.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * This class is used for enabling security configurations.
 *
 * @author Chetan Garikapati
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

    /**
     * Enable spring security does not need any code.
     */
}

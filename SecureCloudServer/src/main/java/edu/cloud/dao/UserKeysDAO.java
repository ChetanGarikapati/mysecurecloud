package edu.cloud.dao;

import edu.cloud.entity.User;
import edu.cloud.entity.UserKeys;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring data repository interface,implementation will be created by spring to handle "user_keys" table.
 * Added custom query methods, add methods here for customizations
 *
 * @author Chetan Garikapati
 */
public interface UserKeysDAO extends JpaRepository<UserKeys, Integer> {
    Optional<UserKeys> findByUserId(User userId);
}

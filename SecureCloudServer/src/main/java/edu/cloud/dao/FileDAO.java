package edu.cloud.dao;

import edu.cloud.entity.FileInfo;
import edu.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring data repository interface,implementation will be created by spring to handle "files" table.
 * Added custom query methods, add methods here for customizations
 *
 * @author Chetan Garikapati
 */
public interface FileDAO extends JpaRepository<FileInfo, Integer> {

    Optional<List<FileInfo>> findAllByUser(User user);

    Optional<FileInfo> findByFileNameAndUser(String fileName, User user);
}

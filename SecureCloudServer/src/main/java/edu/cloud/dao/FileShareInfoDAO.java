package edu.cloud.dao;

import edu.cloud.entity.FileInfo;
import edu.cloud.entity.FileShareInfo;
import edu.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

/**
 * Spring data repository interface,implementation will be created by spring to handle "file_share_info" table.
 * Added custom query methods, add methods here for customizations
 *
 * @author Chetan Garikapati
 */
public interface FileShareInfoDAO extends JpaRepository<FileShareInfo, Integer> {

    Set<FileShareInfo> findByFileId(FileInfo fileId);

    Set<FileShareInfo> findByUserId(User userId);

    Optional<FileShareInfo> findByFileIdAndUserId(FileInfo fileId, User userId);
}

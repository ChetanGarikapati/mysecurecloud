package edu.cloud.dao;

import edu.cloud.entity.Passwords;
import edu.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring data repository interface,implementation will be created by spring to handle "passwords" table.
 * Added custom query methods, add methods here for customizations
 *
 * @author Chetan Garikapati
 */
public interface PasswordDAO extends JpaRepository<Passwords, Integer> {
    Optional<Passwords> findByUserId(User userId);
}

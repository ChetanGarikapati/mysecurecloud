package edu.cloud.dao;

import edu.cloud.entity.AccountInfo;
import edu.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring data repository interface,implementation will be created by spring to handle "account_info" table.
 *
 * @author Chetan Garikapati
 */
public interface AccountInfoDAO extends JpaRepository<AccountInfo, Integer> {

    Optional<AccountInfo> findByUserId(User userId);
}

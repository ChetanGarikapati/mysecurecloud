package edu.cloud.dao;

import edu.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring data repository interface,implementation will be created by spring to handle "users" table.
 * Added custom query methods, add methods here for customizations
 *
 * @author Chetan Garikapati
 */
public interface UserDAO extends JpaRepository<User,Integer> {
    boolean existsByUserName(String username);

    Optional<User> findByUserName(String userName);
}

package edu.cloud.components;

import org.springframework.stereotype.Component;

@Component
public class UserLogin {

    private String userName;
    private String password;
    private String nickName;
    private String privateKeyPassword;

    public UserLogin() {
    }

    public UserLogin(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public UserLogin(String userName, String password, String nickName) {
        this.userName = userName;
        this.password = password;
        this.nickName = nickName;
    }

    public UserLogin(String userName, String password, String nickName, String privateKeyPassword) {
        this.userName = userName;
        this.password = password;
        this.nickName = nickName;
        this.privateKeyPassword = privateKeyPassword;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public void setPrivateKeyPassword(String privateKeyPassword) {
        this.privateKeyPassword = privateKeyPassword;
    }
}

package edu.cloud.components;

import javax.crypto.SecretKey;
import java.io.Serializable;

public class EncryptionContainer implements Serializable {

    private SecretKey secretKey;
    private byte[] IV;
    private byte[] salt;

    public EncryptionContainer(SecretKey secretKey, byte[] IV) {
        this.secretKey = secretKey;
        this.IV = IV;
    }

    public EncryptionContainer() {
    }

    public EncryptionContainer(SecretKey secretKey, byte[] IV, byte[] salt) {
        this.secretKey = secretKey;
        this.IV = IV;
        this.salt = salt;
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }

    public byte[] getIV() {
        return IV;
    }

    public void setIV(byte[] IV) {
        this.IV = IV;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
}

function processLogin() {
    let result = authenticate.performAuthentication($("#emailid").val(), $("#password").val());
    if (!result) alert("Login Failed,Please try Again");
}

function dropBoxQuickEncrypt() {
    encryptanddecrypt.processEncryption("DROPBOX");
}

function driveQuickEncrypt() {
    encryptanddecrypt.processEncryption("GOOGLE_DRIVE");
}

function localQuickEncryption() {
    encryptanddecrypt.processEncryption("Local");
}

function localQuickDecryption() {
    encryptanddecrypt.processDecryption();
}

function updateFileLabel() {
    $("#file-name-label").html($("#file").val().replace(/^.*[\\\/]/, ''));
}

function enablePasswordField() {
    if ($("#enable-password").is(":checked")) {
        $("#file-password").prop("disabled", false)
    } else {
        $("#file-password").prop("disabled", true)
    }
}

function enablePrivatePasswordField() {
    if ($("#enable-privatekey-password").is(":checked")) {
        $("#private-key-password").prop("disabled", false)
    } else {
        $("#private-key-password").prop("disabled", true)
    }
}

function updateLabelFromJava(name) {
    $("#file-name-label").html(name);
}

function secureCloudProcess() {
    let encryptionPasswordForFile = $("#file-password").val();
    let privateKeyPassword = $("#private-key-password").val();
    secureCloudHandle.processUpload(encryptionPasswordForFile, privateKeyPassword);
}

function testFilePicker() {
    secureCloudHandle.promptFileLocation();
}

function signFile() {
    signAndVerify.processSign();
}

function verifyFile() {
    signAndVerify.verifySignature();
}
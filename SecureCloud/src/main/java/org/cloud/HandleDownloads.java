package org.cloud;

import javafx.scene.control.Alert;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Added support to download basic file extensions when available in url.
 *
 * @author Chetan Garikapati
 */
public class HandleDownloads {

    public static final Set<String> DEFAULT_SUPPORTED_EXTENSIONS = new HashSet<>();

    static {
        DEFAULT_SUPPORTED_EXTENSIONS.add("pdf");
        DEFAULT_SUPPORTED_EXTENSIONS.add("zip");
        DEFAULT_SUPPORTED_EXTENSIONS.add("ppt");
        DEFAULT_SUPPORTED_EXTENSIONS.add("exe");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(HandleDownloads.class);

    public static Void processPossibleDownload(String urlLocation, Stage displayStage) {

        String fileExtension = FilenameUtils.getExtension(urlLocation);
        String fileName = FilenameUtils.getBaseName(urlLocation);

        if (!urlLocation.isEmpty() && DEFAULT_SUPPORTED_EXTENSIONS.contains(fileExtension)) {
            DirectoryChooser download = new DirectoryChooser();
            download.setTitle("Select Download Location");
            File downloadFloder = download.showDialog(displayStage);
            try {
                FileUtils.copyURLToFile(new URL(urlLocation), new File(downloadFloder + File.separator + fileName + FilenameUtils.EXTENSION_SEPARATOR + fileExtension));
                Alert downloadComplete = new Alert(Alert.AlertType.INFORMATION, "Download file to " + downloadFloder.getAbsolutePath() + " completed");
                downloadComplete.setHeaderText("File Download Successful");
                downloadComplete.showAndWait();
            } catch (FileNotFoundException e) {
                LOGGER.error("Download failed as file was not found", e);
            } catch (IOException e) {
                LOGGER.error("Download failed", e);
            }
        }
        return null;
    }


}

package org.cloud.securecloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;

/**
 * This class handles upload of file to secure cloud.
 *
 * @author Chetan Garikapati
 */
public class HandleUploadToSecureCloud {

    public static String SECURE_CLOUD_URL;

    private static final Logger LOGGER = LoggerFactory.getLogger(HandleUploadToSecureCloud.class);

    private static void postFile(String filename, byte[] someByteArray, String username, String password, String privateKeyPassword, String filePassword) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setBasicAuth(username, password);


        // This nested HttpEntiy is important to create the correct
        // Content-Disposition entry with metadata "name" and "filename"
        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        ContentDisposition contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename(filename)
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        HttpEntity<byte[]> fileEntity = new HttpEntity<>(someByteArray, fileMap);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileEntity);

        if (filePassword != null && !filePassword.isEmpty()) {
            body.add("filePassword", filePassword);
            body.add("enablePassword", "on");
        }

        if (privateKeyPassword != null && !privateKeyPassword.isEmpty()) {
            body.add("privateKeyPassword", privateKeyPassword);
        }

        HttpEntity<MultiValueMap<String, Object>> requestEntity =
                new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    SECURE_CLOUD_URL,
                    HttpMethod.POST,
                    requestEntity,
                    String.class);

            System.out.println(response.getStatusCode().getReasonPhrase());
        } catch (HttpClientErrorException e) {
            LOGGER.error("Failed upload to securecloud ", e);
        }
    }

    public static void processUpload(File file, String username, String password, String privateKeyPassword, String filePassword) throws IOException {
        FileSystemResource fileSystemResource = new FileSystemResource(file);
        postFile(fileSystemResource.getFilename(), fileSystemResource.getInputStream().readAllBytes(), username, password, privateKeyPassword, filePassword);
    }

    public static void setSecureCloudUrl(String secureCloudUrl) {
        SECURE_CLOUD_URL = secureCloudUrl;
    }
}

package org.cloud.securecloud;

import javafx.scene.control.Alert;
import javafx.scene.web.WebEngine;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This is base class to load secure cloud user details and handle files
 *
 * @author Chetan Garikapati
 */
public class SecureCloudFileProcess {

    private static File fileLocation;
    private final Stage primaryStage;
    private final WebEngine webEngine;

    private Properties secureCloudUserProperties = new Properties();

    private static final String SECURE_CLOUD_DETAILS_FILE = "src\\main\\resources\\securecloud.properties";
    private static final Logger LOGGER = LoggerFactory.getLogger(SecureCloudFileProcess.class);

    private String userName;
    private String password;

    public SecureCloudFileProcess(Stage primaryStage, WebEngine webEngine) {
        this.primaryStage = primaryStage;
        this.webEngine = webEngine;
        loadUserCredentials();
        this.userName = secureCloudUserProperties.getProperty("userName");
        this.password = secureCloudUserProperties.getProperty("password");
    }

    public void promptFileLocation() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select File To Encrypt");
        fileLocation = fileChooser.showOpenDialog(primaryStage);
        webEngine.executeScript("updateLabelFromJava('" + fileLocation.getName() + "')");
        System.out.println(fileLocation);
    }

    public void processUpload(String filePassword, String privateKeyPassword) {
        try {
            HandleUploadToSecureCloud.processUpload(fileLocation, userName, password, privateKeyPassword, filePassword);
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "upload finished");
            alert.showAndWait();
        } catch (IOException e) {
            LOGGER.error("Uploading to secure cloud failed", e);
            Alert alert = new Alert(Alert.AlertType.ERROR, "upload failed");
            alert.showAndWait();
        }
    }

    private void loadUserCredentials() {
        File secureCloudDetailsFile = new File(SECURE_CLOUD_DETAILS_FILE);
        try (InputStream inputStream = new FileInputStream(secureCloudDetailsFile)) {
            secureCloudUserProperties.load(inputStream);
            HandleUploadToSecureCloud.setSecureCloudUrl(secureCloudUserProperties.getProperty("url"));
            LOGGER.info("Loaded secure cloud details : " + secureCloudUserProperties);
        } catch (Exception e) {
            LOGGER.error("Failed loading user details for secure cloud", e);
            new Alert(Alert.AlertType.ERROR, "Failed loading user details for secure cloud," +
                    " operations will be unavilable").showAndWait();
        }
    }
}

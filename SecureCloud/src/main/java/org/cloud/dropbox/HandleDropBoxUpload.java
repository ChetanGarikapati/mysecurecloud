package org.cloud.dropbox;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadErrorException;
import javafx.scene.control.Alert;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * This class deals with handling of uploads to Dropbox.
 *
 * @author Chetan Garikapati
 */
public class HandleDropBoxUpload {

    private static String ACCESS_TOKEN;
    private static final String DEFAULT_TOKEN_FILE_LOCATION = "src\\main\\resources\\dropbox-keys";
    private static final String TOKEN_KEY = "AccessToken";
    private static final String APPLICATION_NAME = "DirectEncryption";

    private static final Alert UPLOAD_FAIL_ALERT = new Alert(Alert.AlertType.ERROR, "Failed uploading,file may already exist in the cloud");
    private static final Logger LOGGER = LoggerFactory.getLogger(HandleDropBoxUpload.class);

    public HandleDropBoxUpload() throws Exception {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(DEFAULT_TOKEN_FILE_LOCATION))) {
            while (bufferedReader.ready()) {
                {
                    String[] split = bufferedReader.readLine().split("-");
                    if (split[1].equalsIgnoreCase(TOKEN_KEY)) {
                        ACCESS_TOKEN = split[0];
                        break;
                    }
                }
            }
        }
    }

    public void uploadToDropbox(String encryptedFileName) {
        try (FileInputStream encryptedFileStream = new FileInputStream(encryptedFileName)) {
            DbxRequestConfig dbxRequestConfig = new DbxRequestConfig(APPLICATION_NAME);
            DbxClientV2 dbxClientV2 = new DbxClientV2(dbxRequestConfig, ACCESS_TOKEN);

            FileMetadata fileMetadata = dbxClientV2.files()
                    .uploadBuilder("/" + FilenameUtils.getBaseName(encryptedFileName) + FilenameUtils.EXTENSION_SEPARATOR + FilenameUtils.getExtension(encryptedFileName))
                    .uploadAndFinish(encryptedFileStream);

            LOGGER.info("Dropbox file Uploaded: " + fileMetadata.getName());
        } catch (FileNotFoundException e) {
            LOGGER.error("File not found for uploading", e);
            UPLOAD_FAIL_ALERT.showAndWait();
        } catch (IOException e) {
            LOGGER.error("Failed reading file for upload", e);
            UPLOAD_FAIL_ALERT.showAndWait();
        } catch (UploadErrorException e) {
            LOGGER.error("Failed uploading file for dropbox", e);
            UPLOAD_FAIL_ALERT.showAndWait();
        } catch (DbxException e) {
            LOGGER.error("Failed uploading file for dropbox", e);
            UPLOAD_FAIL_ALERT.showAndWait();
        }
    }
}

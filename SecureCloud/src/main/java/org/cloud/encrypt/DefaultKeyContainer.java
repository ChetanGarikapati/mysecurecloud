package org.cloud.encrypt;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * This is a container class that holds public and private key for use between classes
 * and is also enclosed in sealed object for secure storage in the file system after encryption
 *
 * @author Chetan Garikapati
 */
public class DefaultKeyContainer implements Serializable {

    private final PrivateKey PRIVATE_KEY;
    private final PublicKey PUBLIC_KEY;


    public DefaultKeyContainer(PrivateKey PRIVATE_KEY,PublicKey PUBLIC_KEY) {
        this.PRIVATE_KEY = PRIVATE_KEY;
        this.PUBLIC_KEY = PUBLIC_KEY;
    }

    public PrivateKey getPRIVATE_KEY() {
        return PRIVATE_KEY;
    }

    public PublicKey getPUBLIC_KEY() {
        return PUBLIC_KEY;
    }

    @Override
    public String toString() {
        return "DefaultKeyContainer{" +
                "PRIVATE_KEY=" + PRIVATE_KEY +
                ", PUBLIC_KEY=" + PUBLIC_KEY +
                '}';
    }
}

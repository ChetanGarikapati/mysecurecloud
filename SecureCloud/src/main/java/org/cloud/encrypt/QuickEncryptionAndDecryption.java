package org.cloud.encrypt;

import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;
import org.cloud.App;
import org.cloud.dropbox.HandleDropBoxUpload;
import org.cloud.google.HandleDriveUploads;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import java.io.File;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Optional;

/**
 * This class handles file selection operations between Java and JavaScript and delegates to appropriate
 * helper classes for further processing.
 *
 * @author Chetan Garikapati
 */
public class QuickEncryptionAndDecryption {

    private Stage primaryStage;
    private App app;
    private HandleDropBoxUpload handleDropBoxUpload;

    private static final String DROPBOX = "DROPBOX";
    private static final String GOOGLE_DRIVE = "GOOGLE_DRIVE";
    private static final Logger LOGGER = LoggerFactory.getLogger(QuickEncryptionAndDecryption.class);

    public QuickEncryptionAndDecryption(Stage primaryStage, App app) {
        this.primaryStage = primaryStage;
        this.app = app;
    }

    public void processEncryption(String cloudSource) {
        String password = null;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select File To Encrypt");
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select Output Directory for Encrypted Files");

        File inputFile = fileChooser.showOpenDialog(primaryStage);
        if (inputFile != null) {
            File outputFolder = directoryChooser.showDialog(primaryStage);
            TextInputDialog passwordDialog = new TextInputDialog();
            passwordDialog.setHeaderText("Enter password for File");
            Optional<String> passwordValue = passwordDialog.showAndWait();

            if (passwordValue.get().isEmpty()) {
                Alert emptyPasswordAlert = new Alert(Alert.AlertType.ERROR, "Empty password");
                emptyPasswordAlert.showAndWait();
            } else {
                password = passwordValue.get();
            }

            try {
                EncryptionContainer encryptionContainer = Encrypt.processEncryption(inputFile.getAbsolutePath(), outputFolder.getAbsolutePath() + File.separator + inputFile.getName() + ".enc");
                if (encryptionContainer != null && !password.isEmpty()) {
                    new Alert(Alert.AlertType.INFORMATION, "Encryption Successful").showAndWait();
                    app.getSecureSaveKeysAndIV().securelySaveKeysAndIV(encryptionContainer, password);
                    if (cloudSource.equalsIgnoreCase(DROPBOX)) {
                        if (handleDropBoxUpload == null) {
                            handleDropBoxUpload = new HandleDropBoxUpload();
                        }
                        handleDropBoxUpload.uploadToDropbox(encryptionContainer.getEncryptedFileName());
                    } else if (cloudSource.equalsIgnoreCase(GOOGLE_DRIVE)) {
                        HandleDriveUploads.uploadFile(encryptionContainer.getEncryptedFileName());
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Encryption Failed", e);
                new Alert(Alert.AlertType.ERROR, "Encryption Failed").showAndWait();
            }
        }

    }

    public void processDecryption() {
        try {
            String password = null;

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select File To Encrypt");
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Select Output Directory for Encrypted Files");

            File encryptedFile = fileChooser.showOpenDialog(primaryStage);
            File decryptedFileDirectory = directoryChooser.showDialog(primaryStage);

            EncryptionIVContainer encryptionIVContainer = HandleKeyStoreOperation.getEncryptionIVContainer();
            KeyStore keyStore = HandleKeyStoreOperation.getKeyStore();

            String decryptedFileName = FilenameUtils.getBaseName(encryptedFile.getAbsolutePath());
            String encryptedFileEntry = decryptedFileName + FilenameUtils.EXTENSION_SEPARATOR + FilenameUtils.getExtension(encryptedFile.getAbsolutePath());

            byte[] IV = encryptionIVContainer.getIV(encryptedFileEntry.toLowerCase());
            if (keyStore.containsAlias(encryptedFileEntry.toLowerCase()) && IV != null) {
                TextInputDialog passwordDecryptDialog = new TextInputDialog();
                passwordDecryptDialog.setHeaderText("Password to Decrypt");
                Optional<String> decryptPassword = passwordDecryptDialog.showAndWait();

                if (decryptPassword.get().isEmpty()) {
                    Alert emptyPassword = new Alert(Alert.AlertType.ERROR, "No password provided");
                    emptyPassword.showAndWait();
                } else {
                    SecretKey secretKey = (SecretKey) keyStore.getKey(encryptedFileEntry, decryptPassword.get().toCharArray());
                    boolean decryptionSuccessful =
                            Encrypt.processDecryption(new EncryptionContainer(decryptedFileDirectory + File.separator + decryptedFileName,
                                    encryptedFile.getAbsolutePath(), secretKey,
                                    encryptionIVContainer.getIV(encryptedFileEntry.toLowerCase())));

                    if (decryptionSuccessful) {
                        new Alert(Alert.AlertType.INFORMATION, "File Successfully Decrypted")
                                .showAndWait();
                    }
                }
            } else {
                new Alert(Alert.AlertType.ERROR, "Decryption keys are not available")
                        .showAndWait();
            }
        } catch (KeyStoreException e) {
            LOGGER.error("Error getting keys from keystore for decryption", e);
        } catch (UnrecoverableKeyException e) {
            LOGGER.error("Error problem with key for decryption", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Invalid algorithm selected for decryption", e);
        }
    }

}

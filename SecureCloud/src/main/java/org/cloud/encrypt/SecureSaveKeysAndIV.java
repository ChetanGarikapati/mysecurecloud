package org.cloud.encrypt;

import org.apache.commons.io.FilenameUtils;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.*;
import java.security.*;
import java.util.Arrays;

import static org.cloud.encrypt.HandleKeyStoreOperation.DEFAULT_IV_STORE_NAME;

public class SecureSaveKeysAndIV {

    private final String keystorePassword;
    private DefaultKeyContainer defaultKeyContainer;
    private EncryptionIVContainer encryptionIVContainer;

    private static final int DEFAULT_KEY_LENGTH = 2048;
    public static final int DEFAULT_SEALING_OBJECT_ENCRYPTION_ROUNDS = 128;
    private static final String DEFAULT_KEY_ALGORITHM = "RSA";

    private static final String DEFAULT_KEYS_FILENAME = "defaultkeys.keys";
    public static final String DEFAULT_SEALING_OBJECT_ALGORITHM = "PBEWithHmacSHA256AndAES_128";


    public SecureSaveKeysAndIV(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public boolean checkDefaultKeyPairs() {

        File ivStoreFile = new File(DEFAULT_IV_STORE_NAME);
        File defaultKeysFile = new File(DEFAULT_KEYS_FILENAME);

        if (defaultKeysFile.exists() && ivStoreFile.exists()) {
            return loadDefaultKeys(keystorePassword);
        } else if (!defaultKeysFile.exists() && ivStoreFile.exists()) {
            return generateDefaultKeyPairs(keystorePassword);
        } else if (defaultKeysFile.exists() && !ivStoreFile.exists()) {
            generateDefaultIVContainer();
            return loadDefaultKeys(keystorePassword);
        } else {
            generateDefaultIVContainer();
            return generateDefaultKeyPairs(keystorePassword);
        }
    }

    public boolean generateDefaultKeyPairs(String defaultKeyPasswords) {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(DEFAULT_KEY_ALGORITHM);
            keyPairGenerator.initialize(DEFAULT_KEY_LENGTH);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();

            defaultKeyContainer = new DefaultKeyContainer(keyPair.getPrivate(), keyPair.getPublic());

            PBEKeySpec pbeKeySpec = new PBEKeySpec(defaultKeyPasswords.toCharArray());
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(DEFAULT_SEALING_OBJECT_ALGORITHM);
            SecretKey sealedObjectKey = secretKeyFactory.generateSecret(pbeKeySpec);
            byte[] salt = new byte[8];
            Arrays.fill(salt, (byte) 0);

            PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, DEFAULT_SEALING_OBJECT_ENCRYPTION_ROUNDS);
            Cipher cipher = Cipher.getInstance(DEFAULT_SEALING_OBJECT_ALGORITHM);
            System.out.println(cipher.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, sealedObjectKey, pbeParameterSpec);

            SealedObject defaultKeys = new SealedObject(defaultKeyContainer, cipher);

            try (ObjectOutputStream defaultKeyStream = new ObjectOutputStream(new FileOutputStream(DEFAULT_KEYS_FILENAME))) {
                defaultKeyStream.writeObject(defaultKeys);
            }

            System.out.println(keyPair.getPublic());
            System.out.println(keyPair.getPrivate());

            return true;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean loadDefaultKeys(String defaultKeyPasswords) {
        try (ObjectInputStream defaultKeyContainerStream = new ObjectInputStream(new FileInputStream(DEFAULT_KEYS_FILENAME))
        ) {
            PBEKeySpec pbeKeySpec = new PBEKeySpec(defaultKeyPasswords.toCharArray());
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(DEFAULT_SEALING_OBJECT_ALGORITHM);
            SecretKey sealedObjectKey = secretKeyFactory.generateSecret(pbeKeySpec);
            byte[] salt = new byte[8];
            Arrays.fill(salt, (byte) 0);

            PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, DEFAULT_SEALING_OBJECT_ENCRYPTION_ROUNDS);
            Cipher cipher = Cipher.getInstance(DEFAULT_SEALING_OBJECT_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, sealedObjectKey, cipher.getParameters());

            SealedObject sealedDefaultKeysObject = (SealedObject) defaultKeyContainerStream.readObject();
            defaultKeyContainer = (DefaultKeyContainer) sealedDefaultKeysObject.getObject(sealedObjectKey);

            if (encryptionIVContainer == null) {
                ObjectInputStream defaultEncryptionIVContainerStream = new ObjectInputStream(new FileInputStream(DEFAULT_IV_STORE_NAME));
                SealedObject sealedEncryptedIVContainer = (SealedObject) defaultEncryptionIVContainerStream.readObject();
                encryptionIVContainer = (EncryptionIVContainer) sealedEncryptedIVContainer.getObject(sealedObjectKey);
                HandleKeyStoreOperation.setEncryptionIVContainer(encryptionIVContainer);

                defaultEncryptionIVContainerStream.close();
            }

            System.out.println(defaultKeyContainer);
            System.out.println(encryptionIVContainer.getContainerInfo());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void securelySaveKeysAndIV(EncryptionContainer encryptionContainer, String password) {
        try {
            String encryptedFileName = FilenameUtils.getBaseName(encryptionContainer.getEncryptedFileName()) + FilenameUtils.EXTENSION_SEPARATOR + FilenameUtils.getExtension(encryptionContainer.getEncryptedFileName());

            KeyStore keyStore = HandleKeyStoreOperation.loadDefaultKeystore(keystorePassword);
            KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(encryptionContainer.getSecretKey());
            KeyStore.PasswordProtection passwordProtection = new KeyStore.PasswordProtection(password.toCharArray());
            keyStore.setEntry(encryptedFileName, secretKeyEntry, passwordProtection);

            encryptionIVContainer.setIV(encryptedFileName.toLowerCase(), encryptionContainer.getInitializationVector());
            System.out.println(encryptionIVContainer.getContainerInfo());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    public void generateDefaultIVContainer() {
        encryptionIVContainer = new EncryptionIVContainer();
        HandleKeyStoreOperation.setEncryptionIVContainer(encryptionIVContainer);
    }

    public void commitKeystoreToDisk() {
        HandleKeyStoreOperation.saveKeyStore(keystorePassword);
    }

    public DefaultKeyContainer getDefaultKeyContainer() {
        return defaultKeyContainer;
    }
}

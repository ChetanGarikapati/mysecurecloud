package org.cloud.encrypt;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This container class is exclusively used for secure storage of IV of files.
 *
 * @author Chetan Garikapati
 */
public class EncryptionIVContainer implements Serializable {

    private ConcurrentHashMap<String, byte[]> encryptedFileIVMap;

    public byte[] getIV(String fileName) {
        if (encryptedFileIVMap != null)
            return encryptedFileIVMap.getOrDefault(fileName, null);
        else
            return null;
    }

    public void setIV(String fileName, byte[] IV) {
        if (encryptedFileIVMap != null)
            encryptedFileIVMap.put(fileName, IV);
        else {
            encryptedFileIVMap = new ConcurrentHashMap<>();
            encryptedFileIVMap.put(fileName, IV);
        }

    }

    public ConcurrentHashMap<String, byte[]> getContainerInfo() {
        return encryptedFileIVMap;
    }
}

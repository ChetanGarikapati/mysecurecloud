package org.cloud.encrypt;

import javax.crypto.SecretKey;
import java.io.Serializable;

/**
 * This is container class to use between classes for holding secret keys, salt and Initialization Vectors
 * These values are also stored in sealed objects except secret key which is stored in Keystore.
 *
 * @author Chetan Garikapati
 */
public class EncryptionContainer implements Serializable {

    private String fileName;
    private String encryptedFileName;
    private SecretKey secretKey;
    private byte[] initializationVector;

    public EncryptionContainer(String fileName,String encryptedFileName, SecretKey secretKey, byte[] initializationVector) {
        this.fileName = fileName;
        this.encryptedFileName = encryptedFileName;
        this.secretKey = secretKey;
        this.initializationVector = initializationVector;
    }

    public EncryptionContainer(){}

    public SecretKey getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }

    public byte[] getInitializationVector() {
        return initializationVector;
    }

    public void setInitializationVector(byte[] initializationVector) {
        this.initializationVector = initializationVector;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getEncryptedFileName() {
        return encryptedFileName;
    }

    public void setEncryptedFileName(String encryptedFileName) {
        this.encryptedFileName = encryptedFileName;
    }

    @Override
    public String toString() {
        return "EncryptionContainer{" +
                "fileName='" + fileName + '\'' +
                ", encryptedFileName='" + encryptedFileName + '\'' +
                '}';
    }
}

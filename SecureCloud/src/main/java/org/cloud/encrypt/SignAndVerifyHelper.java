package org.cloud.encrypt;

import javafx.scene.control.Alert;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

/**
 * This class helps in signing the file with private key and also verify it.
 *
 * @author Chetan Garikapati
 */
public class SignAndVerifyHelper {

    private SecureSaveKeysAndIV secureSaveKeysAndIV;
    private Stage primaryStage;

    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
    private static final String DEFAULT_SIGNED_FILE_NAME_ADDITION = "sign";

    private static final Alert FAILED_SIGN_ALERT = new Alert(Alert.AlertType.ERROR, "Signing the file Failed");
    private static final Alert FAILED_VERIFY_ALERT = new Alert(Alert.AlertType.ERROR, "Signature of the file could not be verified");
    private static final Alert INVALID_SIGN_ALERT = new Alert(Alert.AlertType.ERROR, "Signs of the file did not match");
    private static final Alert SUCCESS_ALERT = new Alert(Alert.AlertType.INFORMATION, "File was signed and saved");
    private static final Alert VALID_SIGN_ALERT = new Alert(Alert.AlertType.INFORMATION, "Signature is valid and verified");

    private static final Logger LOGGER = LoggerFactory.getLogger(SignAndVerifyHelper.class);

    public SignAndVerifyHelper(SecureSaveKeysAndIV secureSaveKeysAndIV, Stage primaryStage) {
        this.secureSaveKeysAndIV = secureSaveKeysAndIV;
        this.primaryStage = primaryStage;
    }

    public void processSign() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file to sign");
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select directory to save signed file");

        File fileToSign = fileChooser.showOpenDialog(primaryStage);
        File directoryToSave = directoryChooser.showDialog(primaryStage);

        String baseName = FilenameUtils.getBaseName(fileToSign.getAbsolutePath());
        Path pathWithSignedFileName = Path.of(directoryToSave.getAbsolutePath(), baseName + FilenameUtils.EXTENSION_SEPARATOR + DEFAULT_SIGNED_FILE_NAME_ADDITION);

        try {
            byte[] rawBytesOfFileToSign = Files.readAllBytes(fileToSign.toPath());
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
            signature.initSign(secureSaveKeysAndIV.getDefaultKeyContainer().getPRIVATE_KEY());
            signature.update(rawBytesOfFileToSign);
            Files.write(pathWithSignedFileName, signature.sign());
            SUCCESS_ALERT.showAndWait();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Invalid algorithm used for signing", e);
            FAILED_SIGN_ALERT.showAndWait();
        } catch (IOException e) {
            LOGGER.error("File could not be opened for signing", e);
            FAILED_SIGN_ALERT.showAndWait();
        } catch (SignatureException e) {
            LOGGER.error("failed signing", e);
            FAILED_SIGN_ALERT.showAndWait();
        } catch (InvalidKeyException e) {
            LOGGER.error("Invalid key was used for signing", e);
            FAILED_SIGN_ALERT.showAndWait();
        }
    }

    public void verifySignature() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file to verify");
        File fileToVerify = fileChooser.showOpenDialog(primaryStage);
        fileChooser.setTitle("Select Signature file to compare");
        File signedFile = fileChooser.showOpenDialog(primaryStage);


        try {
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
            signature.initVerify(secureSaveKeysAndIV.getDefaultKeyContainer().getPUBLIC_KEY());
            signature.update(Files.readAllBytes(fileToVerify.toPath()));
            boolean verify = signature.verify(Files.readAllBytes(signedFile.toPath()));
            if (verify) {
                VALID_SIGN_ALERT.showAndWait();
            } else {
                INVALID_SIGN_ALERT.showAndWait();
            }
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Invalid algorithm used for verifying sign", e);
            FAILED_VERIFY_ALERT.showAndWait();
        } catch (IOException e) {
            LOGGER.error("File could not be opened for verification", e);
            FAILED_VERIFY_ALERT.showAndWait();
        } catch (SignatureException e) {
            LOGGER.error("failed verification", e);
            FAILED_VERIFY_ALERT.showAndWait();
        } catch (InvalidKeyException e) {
            LOGGER.error("Invalid key was used for verification", e);
            FAILED_VERIFY_ALERT.showAndWait();
        }

    }
}

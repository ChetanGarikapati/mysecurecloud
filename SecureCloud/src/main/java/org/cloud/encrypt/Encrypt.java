package org.cloud.encrypt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.SecureRandom;

/**
 * This is a service class for performing  GCM AES encryption and decryption
 *
 * @author Chetan Garikapati
 */
public class Encrypt {

    private static final String ENCRYPTION_ALGORITHM = "AES";
    private static final String CIPHER = "AES/GCM/NoPadding";
    private static final int KEYSIZE = 256;
    private static final int GCM_IV_LENGTH = 12;
    private static final int GCM_TAG_LENGTH = 128;
    private static final int BUFFER_SIZE_LIMIT = 1024;

    private static final Logger LOGGER = LoggerFactory.getLogger(Encrypt.class);


    public static EncryptionContainer processEncryption(String inputFilename, String outputFilename) throws Exception {

        KeyGenerator keyGenerator = KeyGenerator.getInstance(ENCRYPTION_ALGORITHM);
        keyGenerator.init(KEYSIZE);
        SecretKey secretKey = keyGenerator.generateKey();
        SecureRandom secureRandom = new SecureRandom();
        byte[] IV = secureRandom.generateSeed(GCM_IV_LENGTH);

        EncryptionContainer encryptionContainer = new EncryptionContainer(inputFilename, outputFilename, secretKey, IV);
        boolean encryptionSuccessful = encrypt(secretKey, IV, inputFilename, outputFilename);

        if (encryptionSuccessful)
            return encryptionContainer;

        return null;
    }

    private static boolean encrypt(SecretKey secretKey, byte[] IV, String inputFilename, String outputFilename) {

        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(inputFilename));
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(outputFilename));
        ) {
            Cipher cipher = Cipher.getInstance(CIPHER);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), ENCRYPTION_ALGORITHM);
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, IV);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, gcmParameterSpec);

            int available;
            byte[] input;
            while (bufferedInputStream.available() > 0) {
                available = bufferedInputStream.available();
                input = bufferedInputStream.readNBytes(BUFFER_SIZE_LIMIT);

                if (available > BUFFER_SIZE_LIMIT) {
                    byte[] encryptedPartial = cipher.update(input);
                    bufferedOutputStream.write(encryptedPartial);
                } else {
                    byte[] encryptedFinal = cipher.doFinal(input);
                    bufferedOutputStream.write(encryptedFinal);
                }
            }
            return true;
        } catch (Exception e) {
            LOGGER.error("Encryption Failed", e);
            return false;
        }

    }

    public static boolean processDecryption(EncryptionContainer encryptionContainer) {
        return decrypt(encryptionContainer.getSecretKey(), encryptionContainer.getInitializationVector(), encryptionContainer.getEncryptedFileName(), encryptionContainer.getFileName());
    }

    private static boolean decrypt(SecretKey secretKey, byte[] IV, String encryptedFilename, String decryptedOutputFilename) {
        System.out.println(encryptedFilename);
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(encryptedFilename));
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(decryptedOutputFilename))
        ) {
            Cipher cipher = Cipher.getInstance(CIPHER);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), ENCRYPTION_ALGORITHM);
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, IV);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, gcmParameterSpec);

            int available;
            while (bufferedInputStream.available() > 0) {
                available = bufferedInputStream.available();
                byte[] input = bufferedInputStream.readNBytes(BUFFER_SIZE_LIMIT);

                if (available > BUFFER_SIZE_LIMIT) {
                    byte[] decryptedPartial = cipher.update(input);
                    bufferedOutputStream.write(decryptedPartial);
                } else {
                    byte[] decryptedFinal = cipher.doFinal(input);
                    bufferedOutputStream.write(decryptedFinal);
                }
            }
            return true;
        } catch (Exception e) {
            LOGGER.error("Decryption Failed", e);
            return false;
        }

    }


}




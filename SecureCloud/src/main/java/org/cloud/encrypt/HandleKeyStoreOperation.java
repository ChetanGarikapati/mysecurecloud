package org.cloud.encrypt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import static org.cloud.encrypt.SecureSaveKeysAndIV.DEFAULT_SEALING_OBJECT_ALGORITHM;
import static org.cloud.encrypt.SecureSaveKeysAndIV.DEFAULT_SEALING_OBJECT_ENCRYPTION_ROUNDS;

/**
 * This helper class deals with loading and persisting keystore from file system.
 *
 * @author Chetan Garikapati
 */
public class HandleKeyStoreOperation {

    private static final String DEFAULT_KEYSTORE_NAME = "securecloud_keystore.jks";
    public static final String DEFAULT_IV_STORE_NAME = "securecloud_iv_store";
    private static EncryptionIVContainer encryptionIVContainer;
    private static KeyStore keyStore;

    private static final Logger LOGGER = LoggerFactory.getLogger(HandleKeyStoreOperation.class);

    public static boolean checkKeyStoreExistence() {
        try {
            return Files.exists(Path.of(DEFAULT_KEYSTORE_NAME));
        } catch (Exception e) {
            LOGGER.error("Failed to check keystore existence", e);
            return false;
        }
    }

    public static void setEncryptionIVContainer(EncryptionIVContainer encryptionIVContainer) {
        HandleKeyStoreOperation.encryptionIVContainer = encryptionIVContainer;
    }

    public static EncryptionIVContainer getEncryptionIVContainer() {
        return encryptionIVContainer;
    }

    public static KeyStore getKeyStore() {
        return keyStore;
    }

    public static boolean createDefaultKeyStore(String keystorePassword) {

        try (FileOutputStream keystoreOutputStream = new FileOutputStream(DEFAULT_KEYSTORE_NAME)) {
            KeyStore defaultKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
            defaultKeystore.load(null, keystorePassword.toCharArray());
            defaultKeystore.store(keystoreOutputStream, keystorePassword.toCharArray());
            keyStore = defaultKeystore;
            return true;
        } catch (IOException | KeyStoreException e) {
            LOGGER.error("Failed creating and storing keystore", e);
        } catch (CertificateException e) {
            LOGGER.error("Certificate Exception in creating keystore", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Given Algorithm is not available", e);
        }
        return false;
    }

    public static KeyStore loadDefaultKeystore(String keyStorePassword) {
        if (keyStore != null) return keyStore;

        try (FileInputStream keystoreInputStream = new FileInputStream(DEFAULT_KEYSTORE_NAME)) {
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(keystoreInputStream, keyStorePassword.toCharArray());
            return keyStore;
        } catch (IOException | KeyStoreException e) {
            LOGGER.error("Failed loading keystore", e);
            return null;
        } catch (CertificateException e) {
            LOGGER.error("Certificate Exception in loading keystore", e);
            return null;
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Given Algorithm is not available", e);
            return null;
        }

    }

    public static void saveKeyStore(String keystorePassword) {
        if (keyStore != null) {
            try (FileOutputStream keystoreOutputStream = new FileOutputStream(DEFAULT_KEYSTORE_NAME);
                 ObjectOutputStream encryptedIVContainerStream = new ObjectOutputStream(new FileOutputStream(DEFAULT_IV_STORE_NAME));
            ) {
                keyStore.store(keystoreOutputStream, keystorePassword.toCharArray());
                PBEKeySpec pbeKeySpec = new PBEKeySpec(keystorePassword.toCharArray());
                SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(DEFAULT_SEALING_OBJECT_ALGORITHM);
                SecretKey sealedObjectKey = secretKeyFactory.generateSecret(pbeKeySpec);
                byte[] salt = new byte[8];
                Arrays.fill(salt, (byte) 0);

                PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, DEFAULT_SEALING_OBJECT_ENCRYPTION_ROUNDS);
                Cipher cipher = Cipher.getInstance(DEFAULT_SEALING_OBJECT_ALGORITHM);
                cipher.init(Cipher.ENCRYPT_MODE, sealedObjectKey, pbeParameterSpec);

                SealedObject encryptedIvContainer = new SealedObject(encryptionIVContainer, cipher);
                encryptedIVContainerStream.writeObject(encryptedIvContainer);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            }
        }
    }
}

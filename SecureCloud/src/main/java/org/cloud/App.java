package org.cloud;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.cloud.encrypt.HandleKeyStoreOperation;
import org.cloud.encrypt.QuickEncryptionAndDecryption;
import org.cloud.encrypt.SecureSaveKeysAndIV;
import org.cloud.encrypt.SignAndVerifyHelper;
import org.cloud.securecloud.SecureCloudFileProcess;

import java.util.Optional;

/**
 * The base class of the client application
 *
 * @author Chetan Garikapati
 */
public class App extends Application {
    private static final String HOME_PAGE_FILE = "html/index.html";
    private static Pane baseLayout;
    private static WebView mainWebView;
    private static TextField urlBar;
    private static Stage primaryStage;
    private static JSObject javaScriptWindowObject;

    private SecureSaveKeysAndIV secureSaveKeysAndIV;

    private void prepareLayout() {
        TextField urlField = new TextField("HOME");
        Button goToPage = new Button("GO");
        Button goBack = new Button("Back");
        Button goForward = new Button("Forward");
        HBox browserControls = new HBox(goBack, goForward, urlField, goToPage);

        baseLayout.getChildren().add(browserControls);
        VBox vBox = (VBox) baseLayout;
        vBox.setSpacing(5);
    }

    private void prepareWebView() {
        mainWebView = new WebView();
        mainWebView.getEngine().load(getClass().getClassLoader().getResource(HOME_PAGE_FILE).toString());
        baseLayout.getChildren().add(mainWebView);
    }

    private void registerBaseWebViewHandlers() {
        HBox browserControls = (HBox) baseLayout.getChildren().get(0);
        Button goBackButton = (Button) browserControls.getChildren().get(0);
        Button goForwardButton = (Button) browserControls.getChildren().get(1);
        urlBar = (TextField) browserControls.getChildren().get(2);
        Button goToUrl = (Button) browserControls.getChildren().get(3);
        WebView webView = (WebView) baseLayout.getChildren().get(1);

        goBackButton.setOnAction(actionEvent -> {
            webView.getEngine().executeScript("window.history.back()");
        });

        goForwardButton.setOnAction(actionEvent -> {
            webView.getEngine().executeScript("window.history.forward()");
        });

        webView.getEngine().locationProperty().addListener(((observableValue, oldLocation, newLocation) -> {
            urlBar.setText(newLocation);

            //DOWNLOAD FILES WITH EXTENSIONS
            Task<Void> downlaodTask = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    return HandleDownloads.processPossibleDownload(newLocation, primaryStage);
                }
            };

            Platform.runLater(downlaodTask);
        }));

        goToUrl.setOnAction((actionEvent -> {
            webView.getEngine().load(urlBar.getText());
        }));

        urlBar.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                webView.getEngine().load(urlBar.getText());
            }
        });

    }

    private void registerJavaAndJSBindingLogic() {
        mainWebView.getEngine().getLoadWorker().stateProperty().addListener(((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                javaScriptWindowObject = (JSObject) mainWebView.getEngine().executeScript("window");
                javaScriptWindowObject.setMember("encryptanddecrypt", new QuickEncryptionAndDecryption(primaryStage, this));
                javaScriptWindowObject.setMember("handleDownloads", new HandleDownloads());
                javaScriptWindowObject.setMember("secureCloudHandle", new SecureCloudFileProcess(primaryStage, mainWebView.getEngine()));
                javaScriptWindowObject.setMember("signAndVerify", new SignAndVerifyHelper(secureSaveKeysAndIV, primaryStage));
            }
        }));

        //Alert Configurations
        mainWebView.getEngine().setOnAlert(stringWebEvent -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText(stringWebEvent.getData());
            alert.showAndWait();
        });

        primaryStage.widthProperty().addListener((observableValue, old, newValue) -> {
            mainWebView.setPrefWidth(newValue.doubleValue());
            urlBar.setPrefWidth(newValue.doubleValue() - 150);
        });

        primaryStage.heightProperty().addListener((observableValue, old, newValue) -> {
            mainWebView.setPrefHeight(newValue.doubleValue());
        });

    }

    private void prepareHome() {
        prepareLayout();
        prepareWebView();
        registerBaseWebViewHandlers();
        registerJavaAndJSBindingLogic();

        Scene primaryScene = new Scene(baseLayout);
        primaryStage.setTitle("Secure Cloud Application");
        primaryStage.setScene(primaryScene);
        primaryStage.show();

        prepareKeyStore();
    }

    private void prepareKeyStore() {
        if (!HandleKeyStoreOperation.checkKeyStoreExistence()) {
            TextInputDialog keystorePasswordDialog = new TextInputDialog();
            keystorePasswordDialog.setHeaderText("There is no default keystore,enter new password to create one");
            Optional<String> keystorePassword = keystorePasswordDialog.showAndWait();
            if (keystorePassword.get().isEmpty()) {
                new Alert(Alert.AlertType.ERROR, "Password is required").showAndWait();
            } else {
                boolean defaultKeyStore = HandleKeyStoreOperation.createDefaultKeyStore(keystorePassword.get());
                secureSaveKeysAndIV = new SecureSaveKeysAndIV(keystorePassword.get());
                secureSaveKeysAndIV.checkDefaultKeyPairs();
                if (defaultKeyStore) new Alert(Alert.AlertType.INFORMATION, "Keystore Created").showAndWait();
            }
        } else {
            Alert failedUnlockingKeyStoreAlert = new Alert(Alert.AlertType.ERROR, "Loading keystore failed,please check password");
            TextInputDialog unlockKeyStoreDialog = new TextInputDialog();
            unlockKeyStoreDialog.setHeaderText("Enter password to unlock keystore");
            Optional<String> unlockPassword = unlockKeyStoreDialog.showAndWait();
            if (!unlockPassword.get().isEmpty()) {
                HandleKeyStoreOperation.loadDefaultKeystore(unlockPassword.get());
                secureSaveKeysAndIV = new SecureSaveKeysAndIV(unlockPassword.get());
                if (!secureSaveKeysAndIV.checkDefaultKeyPairs()) failedUnlockingKeyStoreAlert.showAndWait();
            } else {
                failedUnlockingKeyStoreAlert.showAndWait();
            }
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        App.primaryStage = primaryStage;
        prepareHome();
    }


    public static void main(String[] args) {
        baseLayout = new VBox();
        launch(args);
    }

    public SecureSaveKeysAndIV getSecureSaveKeysAndIV() {
        return secureSaveKeysAndIV;
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        if (secureSaveKeysAndIV != null) secureSaveKeysAndIV.commitKeystoreToDisk();
    }
}

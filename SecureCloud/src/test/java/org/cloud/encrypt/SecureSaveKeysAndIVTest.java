package org.cloud.encrypt;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;

public class SecureSaveKeysAndIVTest {

    public static void main(String[] args) throws KeyStoreException, UnrecoverableEntryException, NoSuchAlgorithmException {
        SecureSaveKeysAndIV secureSaveKeysAndIV = new SecureSaveKeysAndIV("password");
        //secureSaveKeysAndIV.generateDefaultKeyPairs("password");
        secureSaveKeysAndIV.loadDefaultKeys("password");

        KeyStore keyStore = HandleKeyStoreOperation.loadDefaultKeystore("password");
        System.out.println(keyStore.size());
        System.out.println(keyStore.getEntry("Capture.PNG.enc",new KeyStore.PasswordProtection("password".toCharArray())));
    }
}

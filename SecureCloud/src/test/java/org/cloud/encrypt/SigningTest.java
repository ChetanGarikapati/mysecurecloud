package org.cloud.encrypt;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

public class SigningTest extends Application {

    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
    private static final String DEFAULT_SIGNED_FILE_NAME_ADDITION = "sign";
    private static final SecureSaveKeysAndIV secureSaveKeysAndIV = new SecureSaveKeysAndIV("password");
    private static final Alert FAILED_SIGN_ALERT = new Alert(Alert.AlertType.ERROR, "Signing the file Failed");
    private static final Alert FAILED_VERIFY_ALERT = new Alert(Alert.AlertType.ERROR, "Signature of the file could not be verified");
    private static final Alert INVALID_SIGN_ALERT = new Alert(Alert.AlertType.ERROR, "Signs of the file did not match");
    private static final Alert SUCCESS_ALERT = new Alert(Alert.AlertType.INFORMATION, "File was signed and saved");
    private static final Alert VALID_SIGN_ALERT = new Alert(Alert.AlertType.INFORMATION, "Signature is valid and verified");

    private static Stage primaryStage;

    @Override
    public void start(Stage stage) throws Exception {
        Button signButton = new Button("click to sign");
        signButton.setOnAction(actionEvent -> {
            processSign();
        });

        Button verifyButton = new Button("click to verify");
        verifyButton.setOnAction(actionEvent -> {
            verifySignature();
        });

        HBox hBox = new HBox();
        hBox.getChildren().add(signButton);
        hBox.getChildren().add(verifyButton);

        Scene scene = new Scene(hBox);

        stage.setScene(scene);
        stage.show();

        primaryStage = stage;
    }

    private void processSign() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file to sign");
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select directory to save signed file");

        File fileToSign = fileChooser.showOpenDialog(primaryStage);
        File directoryToSave = directoryChooser.showDialog(primaryStage);

        String baseName = FilenameUtils.getBaseName(fileToSign.getAbsolutePath());
        String extension = FilenameUtils.getExtension(fileToSign.getAbsolutePath());
        Path pathWithSignedFileName = Path.of(directoryToSave.getAbsolutePath(), baseName + FilenameUtils.EXTENSION_SEPARATOR + DEFAULT_SIGNED_FILE_NAME_ADDITION);

        try {
            byte[] rawBytesOfFileToSign = Files.readAllBytes(fileToSign.toPath());
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
            signature.initSign(secureSaveKeysAndIV.getDefaultKeyContainer().getPRIVATE_KEY());
            signature.update(rawBytesOfFileToSign);
            Files.write(pathWithSignedFileName, signature.sign());
            SUCCESS_ALERT.showAndWait();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            FAILED_SIGN_ALERT.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
            FAILED_SIGN_ALERT.showAndWait();
        } catch (SignatureException e) {
            e.printStackTrace();
            FAILED_SIGN_ALERT.showAndWait();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public void verifySignature() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file to verify");
        File fileToVerify = fileChooser.showOpenDialog(primaryStage);
        fileChooser.setTitle("Select Signature file to compare");
        File signedFile = fileChooser.showOpenDialog(primaryStage);


        try {
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
            signature.initVerify(secureSaveKeysAndIV.getDefaultKeyContainer().getPUBLIC_KEY());
            signature.update(Files.readAllBytes(fileToVerify.toPath()));
            boolean verify = signature.verify(Files.readAllBytes(signedFile.toPath()));
            if (verify) {
                VALID_SIGN_ALERT.showAndWait();
            } else {
                INVALID_SIGN_ALERT.showAndWait();
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            FAILED_VERIFY_ALERT.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
            FAILED_VERIFY_ALERT.showAndWait();
        } catch (SignatureException e) {
            e.printStackTrace();
            FAILED_VERIFY_ALERT.showAndWait();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            FAILED_VERIFY_ALERT.showAndWait();
        }

    }

    public static void main(String[] args) {
        System.out.println(secureSaveKeysAndIV.loadDefaultKeys("password"));
        launch(args);

    }
}

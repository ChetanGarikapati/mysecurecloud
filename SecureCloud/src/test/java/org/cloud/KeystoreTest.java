package org.cloud;

import org.cloud.encrypt.HandleKeyStoreOperation;

import java.security.KeyStore;

public class KeystoreTest {

    private static final String ENCRYPTION_ALGORITHM = "AES";
    private static final String CIPHER = "AES/GCM/NoPadding";
    private static final int KEYSIZE = 256;
    private static final int GCM_IV_LENGTH = 12;
    private static final int GCM_TAG_LENGTH = 128;

    public static void main(String[] args) throws Exception {
        boolean checkKeyStoreExistence = HandleKeyStoreOperation.checkKeyStoreExistence();
        System.out.println(checkKeyStoreExistence);

        boolean createNewKeyStore = HandleKeyStoreOperation.createDefaultKeyStore("password");
        System.out.println(createNewKeyStore);

        checkKeyStoreExistence = HandleKeyStoreOperation.checkKeyStoreExistence();
        System.out.println(checkKeyStoreExistence);

        KeyStore defaultKeystore = HandleKeyStoreOperation.loadDefaultKeystore("password");
        System.out.println(defaultKeystore.getType());
    }
}

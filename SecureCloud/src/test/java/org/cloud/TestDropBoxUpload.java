package org.cloud;

import com.dropbox.core.*;
import com.dropbox.core.json.JsonReader;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;

import java.io.*;

public class TestDropBoxUpload {

    private static final String APP_SECRET = "ncwd9xj309q13mq";
    private static final String APP_KEY = "y8v0g9tn85po8yf";

    public static void main(String[] args) throws Exception {
        loginWithAccessToken();
        //normalOAuthFlowAuth();
    }

    private static void loginWithAccessToken() throws JsonReader.FileLoadException, DbxException {

        DbxRequestConfig dbxRequestConfig = new DbxRequestConfig("DirectEncryption");
        DbxClientV2 dbxClientV2 = new DbxClientV2(dbxRequestConfig,"L3L3a7mIURAAAAAAAAAAD2HLH54kxWTO8uUMJlnSbxoBTKgTpcXk3HrqeE1bK3xh");

        System.out.println("Authenticated for user : " + dbxClientV2.users().getCurrentAccount().getName().getDisplayName());
        try (FileInputStream fileInputStream = new FileInputStream("dbxauthinfo.txt")){
            FileMetadata fileMetadata = dbxClientV2.files().uploadBuilder("/dbxauthinfo.txt")
                    .uploadAndFinish(fileInputStream);

            System.out.println(fileMetadata.getSharingInfo());
            System.out.println("load done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void normalOAuthFlowAuth() throws IOException {
        DbxAppInfo dbxAppInfo = new DbxAppInfo(APP_KEY, APP_SECRET);
        DbxRequestConfig dbxRequestConfig = new DbxRequestConfig("DirectEncryption");
        DbxWebAuth dbxWebAuth = new DbxWebAuth(dbxRequestConfig, dbxAppInfo);

        DbxWebAuth.Request webAuthRequest = dbxWebAuth.newRequestBuilder().withNoRedirect().build();

        String authorizeUrl = dbxWebAuth.authorize(webAuthRequest);
        System.out.println(authorizeUrl);
        System.out.println("Enter authorization code");

        String code = new BufferedReader(new InputStreamReader(System.in)).readLine();

        try {
            DbxAuthFinish dbxAuthFinish = dbxWebAuth.finishFromCode(code);
            System.out.println(dbxAuthFinish);
            System.out.println("Access Token: " + dbxAuthFinish.getAccessToken());
            System.out.println("Account Id: " + dbxAuthFinish.getAccountId());
            DbxAuthInfo dbxAuthInfo = new DbxAuthInfo(dbxAuthFinish.getAccessToken(), dbxAppInfo.getHost());

            dbxAuthInfo.Writer.writeToFile(dbxAuthInfo, new File("dbxauthinfo.txt"));
            System.out.println(dbxAuthInfo);
        } catch (DbxException e) {
            e.printStackTrace();
        }
    }
}


# Secure Cloud Application

There are two parts to the application **Secure Cloud Client** and **Secure Cloud Server** which can be used independently.


# Secure Cloud Client

This is an JavaFX application which provides features of encryption/decryption and also supports integration with **Dropbox**, **Google Drive** and hosted **Secure Cloud Server**.

The client can also act as a trusted browser with limited functionality which prevents automatic downloads of files.

## Dropbox Configuration

Login in to the Dropboox App Console and generate the OAuth Access Token and place it in the below folder.
> - src/resources/dropbox-keys
>  - The format should be in the form  **(your_access_token)-AccessToken** without brackets.

## Google Drive Integration

You can integrate with Google Drive by giving access to the enabling the [Drive API](https://developers.google.com/drive/api/v3/quickstart/java).
> - Download your **credentials.json** file and place it in the src/resources folder
Later an encrypted access token for account will be created and placed in **tokens** folder.

## Compiling

The application uses **maven** for dependency management and as build tool.
change directory to application root and where pom.xml file is present and run command 

**Optionally you can compile it as a  executable jar**. 
>  - **mvn clean package**


## Running The application

To start the application run **"App"** class in the target directory using the following command.
> - **java App**


# Secure Cloud Server

This is a web application which encrypts the files on uploading and enables to sharing the files based on public key of the recipient.

## Database  Configurations
The schema will be generated automatically on server startup so only the connection and credentials are required. The application include MySQL drivers and is tested with this database.

The connection and credentials must be placed in the file **"db.properties"** located in **src/main/resources**

## Compiling

The application uses **maven** for dependency management and as build tool.
change directory to application root and where pom.xml file is present and run command .
>  - **mvn clean package**

The compilation will create **war** file in tthe target directory.

## Running the Application

Place the **war** file in any Java Server and make appropriate configurations to run it.
The default context path was configured is **"SecureCloudServer_war"**.

It is recommended that the server is configured to run with **"HTTPS"** as to support 
REST endpoints Http Basic Authentication is enabled.


